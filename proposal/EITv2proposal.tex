\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{tikz}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{float}
\usepackage{enumitem}
\usepackage[margin=10pt,small,bf,tableposition=top,figureposition=bottom,skip=2pt]{caption}
\usepackage[section]{placeins} %figures stay in subsection
\usepackage{placeins}
\let\Oldsection\section       \renewcommand{\section}{\FloatBarrier\Oldsection}
\let\Oldsubsection\subsection \renewcommand{\subsection}{\FloatBarrier\Oldsubsection}

\usepackage[colorlinks,allcolors=blue]{hyperref}

\usetikzlibrary{tikzmark}
\usetikzlibrary{positioning}
%\newcommand\tikzmark[2]{%
%\tikz[remember picture,baseline] \node[inner sep=2pt,outer sep=0] (#1){#2};%
%}

\newcommand\link[2]{%
\begin{tikzpicture}[overlay,remember picture, shorten >=-3pt]
\node [above left=-0.5mm and 5mm of pic cs:#1] (TMPNODEA) {};
\node [above left=-0.5mm and 0mm of pic cs:#1] (TMPNODEB) {};
\node [above left=-0.5mm and 9mm of pic cs:#2] (TMPNODEC) {};
\node [above left=-0.5mm and 5mm of pic cs:#2] (TMPNODED) {};
\draw[thick, ->] (TMPNODEA) -- (TMPNODEB.center) -- (TMPNODEC.center) -- (TMPNODED) ;
%\draw[thick, ->] (TMPNODEA) -- (pic cs:#1) -- (TMPNODEB);
\end{tikzpicture}
}

\newcommand{\im}{\ensuremath{\textbf{i}}}
\newcommand{\figref}[1]{figure~\ref{#1}}
\newcommand{\Figref}[1]{Figure~\ref{#1}}
\newcommand{\secref}[1]{section~\ref{#1}}
\newcommand{\eqnref}[1]{(eqn~\ref{#1})}
\newcommand{\Eqnref}[1]{(Eqn~\ref{#1})}
\newcommand{\JV}{\vec{J}}
\newcommand{\EV}{\vec{E}}
\newcommand{\BV}{\vec{B}}
\newcommand{\xV}{\vec{x}}
\newcommand{\xB}{\mathbf{x}}
\newcommand{\xH}{\hat{\mathbf{x}}}
\newcommand{\xT}{\tilde{\mathbf{x}}}
\newcommand{\yB}{\mathbf{y}}
\newcommand{\yT}{\tilde{\mathbf{y}}}
\newcommand{\vB}{\mathbf{v}}
\newcommand{\DB}{\mathbf{D}}
\newcommand{\IB}{\mathbf{I}}
\newcommand{\MB}{\mathbf{M}}
\newcommand{\RB}{\mathbf{R}}
\newcommand{\PB}{\mathbf{P}}
\newcommand{\QB}{\mathbf{Q}}
\newcommand{\WB}{\mathbf{W}}
\newcommand{\VB}{\mathbf{V}}
\newcommand{\JB}{\mathbf{J}}
\newcommand{\sG}{\bm{\sigma}}
\newcommand{\SG}{\bm{\Sigma}}
\newcommand{\Ew}{\mathop{\rm E}_w}


\begin{document}
\title{Electrical Impedance Tomography:
       Methods, History and Applications, 2$^{\rm nd}$ Ed}
\author{Andy Adler and David S. Holder}
\date{}
\maketitle

{\bf Background:}
EIT has expanded and changed dramatically in the 14 years since the
first edition of this book.
\begin{itemize}[nosep]
\item Publication rate has exploded (see \figref{fig:pubs}), especially
for thoracic applications of EIT.
\item Several companies now produce commercial, medically licensed products
\item There is increasing clinical uptake, along with evidence of
       diagnostically useful EIT results
\item EIT algorithm research has focused on robustness: identifying \& 
       compensating for low-quality data
\item A new field of functional EIT has developed -- analysing EIT images
\end{itemize}

~\\

{\bf Proposal:}
We propose a completely restructured and expanded book, (see \figref{fig:prev_curr}),
 with the following
key contributions
\begin{itemize}[nosep]
\item New sections on ``Image interpretation'' and ``D-bar image reconstruction''
\item New appendices on ``Data and Software'', ``Introduction to lung physiology''
\item A restructuring of the technical material around a
      ``EIT workflow'' (\figref{fig:secEIT-overview}).
\item In each technical section, numerous short ($\frac{1}{2}$--1 page ``Questions'')
      are answered. Each answers applied questions which have commonly been posed,
      in our experience.
      (These questions will supplement the exposition in the regular text).
\item Reproducible data and software. To the extent possible, all illustrations
      will be accompanied by the software to repeat the analysis. For examples
      (see \figref{fig:EITbaby}), we will endeavour to make raw data available.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=.7\columnwidth]{figs/secINT-EITpubs/publications.pdf}
\caption{\label{fig:pubs}%
Number of publications vs.~publication date on
``(Electrical Impedance Tomography OR EIT)''
  published in peer-reviewed journals. (Source: ISI Web of Knowledge, Thomson
 Reuters, New York, USA.)
}
\end{figure}


\begin{figure}
\def\LMARG{nosep,leftmargin=1cm}
\centering
\begin{minipage}[t]{0.45\textwidth}\small
\begin{center}
1$^{\rm st}$ Edition
\end{center}
\begin{enumerate}[nosep,leftmargin=0cm]\setcounter{enumi}{0}
\item[--]
Introduction
\item
Algorithms
\begin{itemize}[nosep,leftmargin=5mm]
\item The Reconstruction Problem\hfill\tikzmark{ba}{~}
\end{itemize}
\item
Hardware
\begin{itemize}[nosep,leftmargin=5mm]
\item EIT Instrumentation \hfill\tikzmark{ca}{~}
\end{itemize}
\item
Applications
\begin{itemize}[nosep,leftmargin=5mm]
\item Imaging of the Thorax by EIT%
      \hfill\tikzmark{da}
\item EIT of Brain Function%
      \hfill\tikzmark{db}
\item Breast Cancer Screening with EIT%
      \hfill\tikzmark{dc}
\item Applications of EIT in the Gastrointestinal Tract (GIT)%
      \hfill\tikzmark{dd}
\item Other Clinical Applications of EIT%
      \hfill\tikzmark{de}
\end{itemize}
\item
New Directions
\begin{itemize}[nosep,leftmargin=5mm]
\item
Magnetic Induction Tomography\hfill\tikzmark{ea}{~}
\item
Magnetic Resonance Electrical Impedance Tomography (MREIT)\hfill\tikzmark{eb}{~}
\item
Electrical Tomography for Industrial Applications\hfill\tikzmark{ec}{~}
\end{itemize}
\item[--] History
\begin{itemize}[nosep,leftmargin=5mm]
\item
EIT: The View from Sheffield\hfill\tikzmark{ha}{~}
\item
EIT for Medical Applications at Oxford Brookes 1985-2003\hfill\tikzmark{hb}{~}
\item
The Rensselaer Experience\hfill\tikzmark{hc}{~}
\end{itemize}
\item[--] Appendices
\begin{enumerate}[label=\Alph*.,nosep,leftmargin=5mm]
\item
 Brief Introduction to Bioimpedance\hfill\tikzmark{za}{~}
\item
 Nontechnical Introduction to EIT\hfill\tikzmark{zb}{~}
\end{enumerate}
\end{enumerate}
\end{minipage}
\hspace{10mm}
\begin{minipage}[t]{0.45\textwidth}\small
\begin{center}
  2$^{\rm nd}$ Edition
\end{center}
\begin{enumerate}[nosep,leftmargin=0cm]\setcounter{enumi}{0}
\item
Introduction
\begin{itemize}[nosep,leftmargin=5mm]
\item
\tikzmark{AA}{%
 EIT intro \& example (Nontechnical)}
\end{itemize}
\item
 EIT: tissue properties to image measures
\begin{itemize}[nosep,leftmargin=5mm]
\item \tikzmark{BA}{Tissue Properties}
\item \tikzmark{BB}{Electronics \& Hardware}
\item \tikzmark{BC}{Sensitivity}
\item \tikzmark{BD}{Image Reconstruction}
\\ -- D-bar image reconstruction
\item \tikzmark{BE}{Image Interpretation}
\end{itemize}
\item
Applications
\begin{itemize}[nosep,leftmargin=5mm]
\item \tikzmark{CA}{Lungs: monitoring ventilation} 
\item \tikzmark{CB}{Lung function} 
\item \tikzmark{CC}{Heart and blood flow} 
\item \tikzmark{CD}{Nervous tissue: brain and nerves} 
\item \tikzmark{CE}{Cancer} 
\item \tikzmark{CF}{Gastrointestinal} 
\item \tikzmark{CG}{Other Clinical Applications} 
\end{itemize}
\item
New Directions
\begin{itemize}[nosep,leftmargin=5mm]
\item \tikzmark{DA}{%
Magnetic Induction Tomography}
\item \tikzmark{DB}{%
Magnetic Resonance EIT}
\end{itemize}
\item History
\begin{itemize}[nosep,leftmargin=5mm]
\item \tikzmark{EA}{%
Pioneers: Sheffield, Rensselaer, etc.}
\item
Clinical: G\"ottingen, Kiel, etc.
\item
Companies: Maltron, Viasys, Dr\"ager, Dixtal/Timpel, Swisstom:
\end{itemize}
\item[--] Appendices
\begin{enumerate}[label=\Alph*.,nosep,leftmargin=5mm]
\item
 Software and Data for EIT
\item
 Review of lung physiology
\item
 Geophysical ERT
\item \tikzmark{EC}{%
 Process tomography}
\end{enumerate}
\end{enumerate}
\end{minipage}
\link{zb}{AA}
\link{za}{BA}
\link{ba}{BC}
\link{ba}{BD}
\link{ca}{BB}
\link{da}{CA}
\link{da}{CB}
\link{da}{CC}
\link{db}{CD}
\link{dc}{CE}
\link{dd}{CF}
\link{de}{CG}
\link{ha}{EA}
\link{hb}{EA}
\link{hc}{EA}
\link{ec}{EC}
\link{eb}{DB}
\link{ea}{DA}
\caption{\label{fig:prev_curr}%
Comparison of 1$^{\rm st}$ and proposed 2$^{\rm nd}$ edition structure.
New sections include ``Image interpretation'', ``D-bar image reconstruction'',
and history of clinical-lung applications and companies.
Thoracic EIT is greatly expanded into three sections, as well 
as a review of lung physiology.
}
\end{figure}

\newpage
\tableofcontents
\newpage
\section{Section: Introduction}
\subsection{Nontechnical introduction to EIT}
\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{figs/secINT-baby-figure/EIT-baby.pdf}
\caption{\label{fig:EITbaby}
A 10-day old infant with EIT electrodes, from the study [Heinrich et al, 2006].
% \cite{Heinrich2006Infants}.
from which cross-sectional images are reconstructed, with pixel waveforms
showing the heart and left and right lung activity. For an infant with the
head turned to the side, the
contralateral lung receives most tidal ventilation.
 \href{http://eidors3d.sourceforge.net/tutorial/EIDORS_basics/neonate_intro.shtml}
      {[Software and Data]}
}
\end{figure}
\subsection{Bioimpedance and tissue properties}

\newpage
\section{Section: EIT: tissue properties to image measures}

\begin{figure}[h]\centering
\includegraphics[width=\textwidth]{figs/secEIT-overview/secEIT-overview.pdf}
\caption{\label{fig:secEIT-overview}
Overview of the steps in EIT image generation and interpretation.
A:
physiologically interesting tissues have
 electrical properties which contrast from surrounding
tissues or change over time;
B:
EIT systems hardware applies electrical current and measures
voltages on a sequence of body surface electrodes;
C:
The pattern of electrical stimulation and measurements,
and the body geometry and electrical properties,
determine the sensitive region;
D:
Images of the conductivity (absolute EIT) or
the change in conductivity (difference EIT) are
calculated from using the sensitivity field using
an image reconstruction algorithm;
and
E:
Physiologically relevant measures are calculated
from image contrasts and their changes over time.
}
\end{figure}

\newpage
\subsection{Tissue Electrical Properties}

\subsubsection{Question:
How much does frequency response of biological tissues vary
over below 1\,MHz?
}

\subsubsection{Question:
What frequency ``should'' an EIT system run at?
}

\subsubsection{Question:
Is resistivity linear with lung gas volume?
}

\subsubsection{Question:
How important is anisotropy?
}

\subsubsection{Question:
How do nerves change conductivity when active?
}

\subsubsection{Question:
What are the bulk properties of a mixture of tissue?
}

\subsubsection{Question:
Conductivity of moving blood
}



\newpage
\subsection{Electronics \& Hardware}

\subsubsection{Question:
How ``good'' does EIT hardware have to be?
}

\subsubsection{Question:
How fast can an EIT system go?
}

\subsubsection{Question:
Do we need shielded cables?
}

\subsubsection{Question:
Do we need to worry about electrode polarization?
}

\subsubsection{Question:
How serious is CMRR?
}

\subsubsection{Question:
How serious is cross-talk?
}

\subsubsection{Question:
What are limits to EIT safety
}

\newpage
\subsection{The forward problem (EIT Sensitivity)}


\subsubsection{Question: How does sensitivity vary with conductivity}
\begin{figure}[H]\centering
\includegraphics[width=\columnwidth]{figs/secEIT-C.conductivity-contrast/conductivity_contrast.pdf}
\caption{%
\label{fig:secEIT-C.conductivity-contrast}
The relative EIT sensitivity, $S(\sigma_c)$ as a function of the shape and
conductivity of a ROI. The stimulation configuration (subfigure at right)
has 16 electrodes in a central plane (dotted), and a
has a contrasting cylindrical ROI with a height/diameter, $h$, and conductivity
$\sigma_c$ while elsewhere $\sigma=1$.
The graph shows the
normalized EIT signal, $S(\sigma_c)$, as a function of
$\sigma_c$ for four values of $h$.
 \href{http://eidors3d.sourceforge.net/tutorial/EIDORS_basics/contrasts.shtml}
      {[Software and Data]}
}
\end{figure}

\subsubsection{Question:
Interpretation of sensitivity matrix, $\JB$
}

\subsubsection{Question:
How important is electrode size?
}

\subsubsection{Question:
Contact impedance models
}

\subsubsection{Question:
Modelling hardware and cable properties
}


\subsubsection{Question:
Off-plane sensitivity of EIT
}

\subsubsection{Question:
Is there a benefit to having internal electrodes?
}

\subsubsection{Question:
Comparing adjacent vs.\ ``skip'' patterns
}

\subsubsection{Question:
Comparing ``trig'' vs.\ pair-drive patterns
}

\subsubsection{Question:
How accurate should FEMs be?
}

\subsubsection{Question:
How to model EIT in the time domain
}

\subsubsection{Question:
When do we need to use a ``full Maxwell'' solver
}

\subsubsection{Question:
Some useful analytic models
}

\subsubsection{Question:
Relationship between deformation and anisotropy
}

\subsubsection{Question:
Understanding FEM as a resistor mesh
}

\subsubsection{Question:
What shapes is EIT most sensitive to?
}

\subsubsection{Question:
Perturbation approaches to calculating $\JB$
}

\subsubsection{Question:
Measuring $\JB$ from hardware
}


\newpage
\subsection{The inverse problem (Image Reconstruction)}

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{figs/secEIT-D.reconstruction-blockdia/image-reconstruction.pdf}
\caption{%
Schema for image reconstruction based on model fitting. EIT data, $\yB$, are
measured with an instrument from body $\Omega$. Iteratively, a model $\xB_k$
is improved by updates, $\Delta\xB$, calculated from the mismatch between
the current forward estimate and sensitivity, and a prior model.
The mapping between image
parameters, $\xB$, of a planar slice, and the 3D FEM-based model are
illustrated.
}
\label{fig:imageRecSchema}
\end{figure}

outline:
\begin{itemize}
\item overview of regularized schemes
\item difference EIT
\item absolute EIT
\item other approaches - including D-bar
\end{itemize}

\subsubsection{Question:
Why doesn't lung EIT use 3D electrode placements
}

\subsubsection{Question:
How many electrodes do we need?
}

\subsubsection{Question:
Advice: look at the raw data?
}

\subsubsection{Question:
Why is absolute EIT so hard?
}

\subsubsection{Question:
Normalized difference EIT
}

\subsubsection{Question:
How to normalize frequency-difference EIT data
}

\subsubsection{Question:
Image reconstruction and parameterization
}

\subsubsection{Question:
How does Sheffield backprojection compare to modern algorithms
}

\subsubsection{Question:
How to choose an algorithm hyperparameter
}

\subsubsection{Question:
Detecting electrode errors
}

\subsubsection{Question:
Managing electrode errors
}

\subsubsection{Question:
What do they do differently in geophysical ERT
}

\newpage
\subsection{Image Interpretation}

Image interpretation is functional EIT images + EIT measures.

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{figs/secEIT-E.image-interpretation-ex/fEIT-vs-measures.pdf}
\caption{\label{fig:fEITvsMeasures}%
Calculation of functional EIT images and measures from an
EIT image sequence (A). In (B) waveforms are analysed
to calculate a tidal variation (TV) parameter, from
which fEIT images (C) are generated. In (D), a horizontal
histogram of breathing in ``slices'' of the left and
right lung is calculated, from which the centre of
gravity (E) in the left and right lung are determined.
}
\end{figure}

\newpage
\section{Applications}
\subsection{Lungs: monitoring ventilation} 
\subsection{Lung function} 
\subsection{Heart and blood flow} 
\subsection{Nervous tissue: brain and nerves} 
\subsection{Cancer} 
\subsection{Gastrointestinal} 
\subsection{Other Clinical Applications} 

\newpage
\section{New Directions}
\subsection{Magnetic Induction Tomography}
\subsection{Magnetic Resonance EIT}

\newpage
\section{History}
\subsection{Pioneers: Sheffield, Rensselaer, etc.}
\subsection{Clinical: G\"ottingen, Kiel, etc.}
\subsection{Companies: Maltron, Viasys, Dr\"ager, Dixtal/Timpel, Swisstom}

\newpage
\section{Appendices}
\subsection{Software and Data for EIT}
\subsection{Review of lung physiology}
\subsection{Geophysical ERT}
\subsection{Process tomography}


\end{document}
