\chapter[Monitoring of Ventilation]{%
Electrical Impedance Tomography for monitoring of ventilation
}
\chapterauthor{
Tobias Becher
}

\begin{center}\begin{tabular}{lp{10cm}}
 & Task List Chapter \\\hline
   \checkmark& Send to author for comments
\\ \checkmark& Confirm rights for figures
\\ \checkmark& Implement author changes
\\ --        & Confirm affiliations
\\ \checkmark& Move bibliography to book references
\end{tabular}\end{center}

\def\Crs{$\rm C_{rs}$}
\def\Re{$\rm R_e$}

\newcommand{\ADVDISPRAC}[1]{{\noindent \bf #1}}


\section{%
Introduction
}


Patients suffering from acute respiratory failure are frequently treated with
positive pressure ventilation in interdisciplinary intensive care units (ICUs).
Due to the severity of their illness and the effect of sedative medications
that may be necessary during the course of mechanical ventilation, these
critically ill patients are often unable to communicate with their caregivers.
Moreover, the lungs of critically ill patients may suffer further damage due to
the potentially detrimental effects of ventilator-induced lung injury (VILI,
\cite{Slutsky2013Ventilator} and sometimes patient self-inflicted lung injury
(P-SILI, \cite{Brochard2017Mechanical}). Therefore, precise monitoring is
essential for early detection of changes in the patient’s condition as well as
for individual adaptation of care. Conventional monitoring of ventilation in
non-ventilated ICU patients comprises continuous monitoring of oxygen
saturation with pulse oximetry and monitoring of respiratory rate by thoracic
impedance measurements through electrocardiography (ECG) electrodes. In
mechanically ventilated patients, the above-mentioned conventional monitoring
parameters are commonly complemented by monitoring of airway pressure, tidal
volume and expired carbon dioxide. These parameters provide important
information about the effectiveness and invasiveness of ventilation on a global
scale. 

The lungs of patients with respiratory failure, however, are frequently
characterized by substantial local inhomogeneities of ventilation distribution
which are caused by variable distributions of atelectasis (partial or
complete), normally aerated lung and overdistended areas. Regional
overdistension and cyclic opening and closing of atelectatic lung areas are
potentially dangerous phenomena as they may aggravate the pre-existing lung
injury. Since all conventional monitoring methods for ventilation rely on
global measures of respiratory mechanics and gas exchange, they are not
suitable for identifying regional inhomogeneities and for adjusting ventilator
parameters accordingly. 

In the recent years, chest EIT has received increasing attention as a
non-invasive tool for continuous visualization of ventilation distribution and
for assessment of ventilation-induced changes in lung volume. EIT provides
reliable bed-side information on ventilation distribution 
\cite{Frerichs2002Detection, Wrigge2008Electrical}
and its homogeneity \cite{Zhao2010PEEP}.
Moreover, when used in conjunction with specific ventilator maneuvers and
information derived from the ventilator, EIT can identify regional
overdistension, atelectasis formation and cyclic opening and closing of lung
tissue at the bedside \cite{Costa2009Bedside, Muders2012Tidal,
Zick2013Effect}.

In the following, we will discuss different EIT measures that can be used to
assess the lungs of critically ill and mechanically ventilated patients at the
bedside. Furthermore, we will describe potential strategies for prospective
individualization and, ideally, optimization of ventilator settings using these
EIT parameters. 

\section{%
Assessment of ventilation distribution with EIT
}

Changes in electrical bioimpedance during respiration are closely correlated to
changes in pulmonary air content \cite{Frerichs2002Detection}.
With current EIT devices, ventilation distribution is commonly assessed based
on tidal images displaying the pixel impedance differences between
end-expiration and end-inspiration. This information can be displayed
graphically as tidal images, which are the basis for some by now
well-established EIT parameters describing ventilation distribution in a
quantitative way. 

By dividing the anteroposterior and the right-to-left chest diameter in their
respective middles into one ventral and one dorsal as well as a right and a
left half, the ventral, dorsal, right and left-sided percentages of ventilation
can be expressed. These measures are relatively inaccurate but provide
intuitive information for treating clinicians, which is why they have been used
in several clinical studies \cite{Mauri2015Effects,Mauri2017Optimum}
and, among other parameters, have been considered as generally ``useful'' in a
survey on clinical usefulness of different EIT parameters
\cite{Frerichs2019Chest}.
An example for adjustment of ventilator settings using the dorsal
percentage of ventilation is given in \figref{fig:ch9:1VentDistribution}.

\begin{figure}[h]\centering
\includegraphics[width=0.8\columnwidth]
   {\CURRCHAP/fig-1VentDistribution/Figure1_VentilationDistribution_PEEP10-15.pdf}
\FIGCAPTION{fig:ch9:1VentDistribution}{\CURRCHAP/fig-1VentDistribution}
\end{figure}


The center of gravity of ventilation distribution is another way of expressing
anteroposterior or right-to-left ventilation distribution. It has a value
between 0 and 1 and provides more accurate information on ventilation
distribution than the dorsal / ventral or right / left percentages of
ventilation but may be less intuitive to clinicians at the bedside 
(\cite{Frerichs1998Monitoring}, \cite{Frerichs2017Chest}).


\section{%
Measures of ventilation inhomogeneity
}

In addition to the above-mentioned measures of ventilation distribution, tidal
images can be used to quantify the homogeneity of ventilation distribution
using measures like the global inhomogeneity index (GI index) or the
coefficient of variation (CV)
(\cite{Zhao2010PEEP}, \cite{Becher2016Functional}).
In general, higher values of these parameters denote greater degrees of
inhomogeneity in ventilation distribution, as assessed by the tidal image that
is the basis for this calculation. 

For a meaningful interpretation of inhomogeneity and changes in inhomogeneity
using the aforementioned indices, it is necessary to define the lung area
within the tidal image. Failure to do so will result in very high values of GI
index and CV that are not necessarily caused by a high degree of ventilation
inhomogeneity but instead by the differences between the ventilated lung pixels
and other intra- and extra-thoracic pixels that do not represent the ventilated
lung but are still part of the tidal image, although an improved
``anatomical'' GA metric (GA$_{\rm anat}$) has recently been proposed
\cite{Yang2020Lung}.

Various methods for determining the lung area have been proposed. Functional
regions of interest (fROIs) define the lung area by analyzing the functional
behavior of pixels during ventilation. For example, an fROI of 10\% of maximum
tidal impedance change defines all pixels as ``lung area'' which exhibit a
ventilation-related impedance change of more than 10\% of the maximum
ventilation-related impedance change within the image. This definition
automatically excludes all non-ventilated pixel from the lung area, which is
desired for extrapulmonary extrapulmonary regions but not desired for pixels
belonging to the actual lung area that are not ventilated because of
atelectasis or severe overdistension. By automatically excluding these
non-ventilated lung areas from the analysis, fROIs may lead to an
underestimation of actual ventilation inhomogeneity. To overcome this
limitation, Zhao et al.\ introduced the lung area estimation method, which is
based on the assumption that lung dimensions should be relatively symmetric
between the right and left lung in the EIT electrode plane (\cite{Zhao2010PEEP}).
Obviously, this is only a rough approximation that may lead to an
underestimation of the actual lung area, especially in patients with bilateral
atelectases. In the future, individual anatomical regions of interest based on
computed tomography (CT) scans may help to detect the actual lung area before
EIT data analysis. Until then, measures of ventilation inhomogeneity must be
interpreted with extreme caution, bearing in mind the possibility of
misinterpretations due to the dependency of GI index and CV on the regions of
interest used for these analyses. 


\section{%
Intratidal ventilation inhomogeneity and alveolar cycling
}

When calculated from tidal images, GI index and CV describe the homogeneity of
ventilation distribution at the time point of end-inspiration. Lung areas that
are fully inflated at the time of end-inspiration will contribute to lower
values of these indices, even if they are subject to alveolar cycling during
inspiration. Therefore, it is important to obtain information on homogeneity of
ventilation distribution not only during the comparatively static time point of
end-inspiration but also during the course of inspiration, when alveolar
cycling takes place. The GI index can be used to assess intratidal
inhomogeneity during a slow inflation maneuver \cite{Zhao2014EIT}. If
the fully inflated lung region (based on EIT data obtained at the end of the
slow inflation maneuver) is used as region of interest for calculation of GI
index, relatively high values can be observed at the beginning of the maneuver,
when a low airway pressure leads to a partial inflation of the lungs. During
the maneuver, the increasing airway pressure leads to increased lung inflation.
This translates into a progressive decrease of GI index from the initially high
values to lower values that can be observed at end-inspiration. 

The standard deviation of regional ventilation delay (SDRVD,
\cite{Muders2012Tidal} assessed during a slow inflation maneuver is another
measure for intratidal ventilation inhomogeneity. It describes the temporal
inhomogeneity of lung filling and is closely correlated to alveolar cycling.
Higher values of SDRVD can be found when a large proportion of alveoli are
closed at end-expiration and opened at end-inspiration. Of note, the close
relationship between SDRVD and alveolar cycling was found only when analyzing a
slow-inflation maneuver, during which the lung was slowly inflated with reduced
air flow up to an inspiratory tidal volume of 12\,ml/kg. During such a maneuver,
there is a slow and progressive increase in airway pressure throughout the
course of lung inflation. Lung units that are collapsed at end-expiration will
typically require a higher airway pressure for opening, which results in
delayed inflation during such a maneuver and hence higher values of SDRVD. It
is questionable whether SDRVD, when calculated during conventional mechanical
ventilation with rapid increases in airway pressure during inspiration will
exhibit any correlation with alveolar cycling, because the rapid increases in
airway pressure, especially during pressure-controlled ventilation, may lead to
rapid opening of previously collapsed lung units during inspiration, masking
any heterogeneity in regional opening pressures that could be detected with
SDRVD.  Therefore, we recommend using a slow inflation maneuver for
calculation of SDRVD because the results during ongoing mechanical ventilation
may be unreliable for assessment of alveolar cycling.
\Figref{fig:ch9:3SDRVD} presents a
patient example of SDRVD before after raising the PEEP setting on the
ventilator from 8 to 14\,mbar.

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]
   {\CURRCHAP/fig-3SDRVD/Figure3_SDRVD_PEEP8-14.pdf}
\FIGCAPTION{fig:ch9:3SDRVD}{\CURRCHAP/fig-3SDRVD}
\end{figure}

\section{%
Identification of overdistension and alveolar collapse at the bedside using
regional compliance estimation
}

In 2009, Costa and coworkers published a technical note presenting a novel
EIT-based method for estimating recruitable alveolar collapse and
overdistension at the bedside (\cite{Costa2009Bedside}). Their method is based on
calculation of pixel compliances during a stepwise reduction in positive
end-expiratory pressure (PEEP). During this maneuver, which is frequently
referred to as ``decremental PEEP trial'', the PEEP level is initially set to the
highest value possible (typically in the range of 20 to 24\,mbar) and then
reduced in small steps of 2 or 3\,mbar that are conducted every minute until a
very low level of PEEP (typically around 5\,mbar) is reached. The maneuver may
be conducted in volume-controlled or pressure-controlled mode. To yield valid
results, a precise assessment of respiratory system compliance (\Crs) must be
ensured. This typically requires neuromuscular paralysis or deep sedation to
eliminate any spontaneous breathing activity that might interfere with a valid
assessment of \Crs. For every PEEP level investigated, the maneuver yields one
value for relative overdistension and one value for relative alveolar collapse.
Typically, the ``best'' PEEP is considered to be the PEEP level where both
alveolar overdistension and collapse are minimized.
\Figref{fig:ch9:4Costa} presents the
results of a decremental PEEP trial analyzed according to this approach.

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]
   {\CURRCHAP/fig-4Costa/Figure4_Costa.pdf}
\FIGCAPTION{fig:ch9:4Costa}{\CURRCHAP/fig-4Costa}
\end{figure}



Since its publication, this approach has become one of the most widely used
methods for bedside adjustment of positive end-expiratory pressure (PEEP) with
EIT.
It was successfully applied in patients with respiratory failure (\cite{Heines2019Clinical}),
on extracorporeal membrane oxygenation (\cite{Franchineau2017Bedside})
and suffering from COVID-19 (\cite{Sella2020Positive}, \cite{vanderZee2020Electrical}).
In a study including 40 intraoperative patients ventilated during
elective abdominal surgery, adjustment of PEEP with the ``Costa-Approach'' led to
improved intraoperative oxygenation and reduced postoperative atelectasis
(\cite{Pereira2018Anesthesiology}).

The ability of the ``Costa-Approach'' to identify the PEEP level associated with
the ``best compromise'' between alveolar overdistension and collapse makes it
appealing for clinical use, whenever finding the best PEEP level for an
individual patient is challenging. Despite these advantages, the required
decremental PEEP trial is a time-consuming ventilation maneuver that requires
adjusting PEEP to a level that is presumably too high for most patients at the
beginning, and then reducing it to a level that may be too low for most
patients at the end of the maneuver. It is only after performing such a
ventilation maneuver that the ``best'' PEEP for an individual patient can be
identified and, subsequently, used for further therapy. The dynamics of
critical illness and mechanical ventilation, however, may require frequent
adjustment of PEEP on an hourly or, at the very least, daily basis. In clinical
practice, when patients are already ventilated with a previously selected PEEP
level, the clinical question is frequently whether it is safe to reduce PEEP
by, e.g., 2--3\,mbar or whether it should instead be maintained or even increased
by a similar figure. Performing a complete decremental PEEP trial every time
that an adjustment of PEEP is required is not feasible and may be potentially
dangerous. 

This frequently encountered clinical challenge may be addressed by performing a
brief variation in PEEP or tidal volume (VT) and by analyzing its effects on
regional \Crs (\cite{Zick2013Effect}, \cite{Becher2014Assessment}).
If a reduction in VT leads to a decrease in regional \Crs, this may be
interpreted as being caused by an end-expiratory collapse and delayed opening
of the corresponding lung areas.  In this case, an elevation of PEEP to
overcome this ``alveolar cycling'' would be advisable. If, on the other hand, a
brief reduction in VT leads to an increase in regional \Crs, this may be
interpreted as relative overdistension of the corresponding lung area with the
previously applied VT. The clinical consequence, in this scenario, would be to
decrease PEEP or VT to reduce this overdistension. 

Especially in patients with acute respiratory distress syndrome, end-expiratory
collapse and overdistension occur at the same time but in different lung
regions (\cite{Becher2019Changes}). This clinically challenging situation can be
addressed by increasing PEEP to counteract end-expiratory collapse and by
simultaneously decreasing VT to counteract end-inspiratory overdistension.


\section{%
Identification of poorly ventilated lung areas
}

While EIT allows reliable identification of ventilated lung areas by means of
the ventilation-associated impedance changes that can be detected in the
ventilated lung, identification of non-ventilated or poorly ventilated lung
areas may be more challenging. In principle, lung areas may become
non-ventilated or poorly ventilated due to complete alveolar collapse at low
airway pressure or due to severe overdistension caused by excessive airway
pressure. In both cases, these areas exhibit no detectable impedance change
making them difficult to distinguish from extrapulmonary thoracic tissue. 
Thus, a pixel exhibiting little or no tidal impedance change in the functional
EIT image can be atelectatic, severly overdistended or may be located outside
the anatomical lung region.

Distinguishing these situations requires prior knowledge of the presumed lung
area within the EIT image. This information can be derived from a CT image of
the patient investigated or, more pragmatically, by obtaining the lung contours
from a previously established database of three-dimensional thoracic models
created from computed tomography scans of different patients, taking into
account the patient’s demographic characteristics \cite{Ukere2016Perioperative}.
If the presumed lung contours are known, any pixel within the lung contours
that exhibits no or very little tidal impedance variation can be classified as
``low tidal variation region'' (\cite{Frerichs2017Chest}) or
``Silent Space'' \cite{Ukere2016Perioperative}.
Silent Spaces in the gravitationally non-dependent lung regions may
be interpreted as being caused by overdistension, whereas Silent Spaces in the
gravitationally dependent lung regions may be interpreted as being caused by
atelectasis. In a study including 14 patients with acute respiratory failure,
Spadaro and colleagues demonstrated a correlation between changes in dependent
Silent Spaces and lung recruitment measured using the pressure-volume-loop
technique (\cite{Spadaro2018Variation}).
Non-dependent Silent Spaces, however, may be
less sensitive for detection of overdistension with higher PEEP. An example of
the influence of higher PEEP on Silent Spaces is given in 
\figref{fig:ch9:2CoV}.

\begin{figure}[h]\centering
\includegraphics[width=0.7\columnwidth]
   {\CURRCHAP/fig-2CoV/Figure2_CoV_SS.pdf}
\FIGCAPTION{fig:ch9:2CoV}{\CURRCHAP/fig-2CoV}
\end{figure}




\section{%
End-expiratory lung impedance changes for quantification of lung recruitment and derecruitment
}

Images of end-expiratory lung impedance change display the regional impedance
differences between different breaths comparing the time points of
end-expiration. As tidal impedance differences are closely correlated to
regional tidal volume, it has been postulated that changes in end-expiratory
lung impedance between different breaths may be used to quantify regional
changes in end-expiratory lung volume. A study comparing changes in
end-expiratory lung volume as determined by the Helium dilution method to
changes in end-expiratory lung impedance found an acceptable agreement between
both methods (\cite{Mauri2016Bedside}).
In a feasibility study, the time course of
end-expiratory lung impedance following a brief recruitment maneuver was
utilized to determine whether the applied PEEP level was sufficient to maintain
alveolar recruitment (\cite{Eronia2017Bedside}).
Despite these promising results,
it must be highlighted that end-expiratory lung impedance may be influenced by
other factors than changes in endexpiratory lung volume. Pulsation therapy with
inflatable mattresses, which is a rather common intervention in today’s
intensive care units, can cause substantial changes in end-expiratory impedance
levels that cannot be explained by changes in lung volume (\cite{Frerichs2011Patient}).
The same applies to patient movement and changes in torso and arm
position (\cite{Vogt2016Influence}).
Even intravenous fluid therapy, which is applied
to mechanically ventilated patients on a regular basis, may cause substantial
changes in end-expiratory lung impedance
(\cite{Becher2019Residual}, \cite{Sobota2019Intravenous}).
To avoid serious misinterpretations, such possible sources of
interference must always be considered when assessing changes in end-expiratory
lung volume by observing the time course of end-expiratory lung impedance.
\Figref{fig:ch9:6dEELI} presents two examples of the time course of end-expiratory lung
impedance after a decrease in PEEP (panel A) and a recruitment maneuver (panel
B). 

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]
   {\CURRCHAP/fig-6dEELI/Figure6_dEELI.pdf}
\FIGCAPTION{fig:ch9:6dEELI}{\CURRCHAP/fig-6dEELI}
\end{figure}



Expiratory time constants for monitoring airflow limitation Some respiratory
diseases like Asthma and chronic obstructive pulmonary disease (COPD) are
characterized by delayed lung emptying caused by expiratory airflow
obstruction.  By approximation, the time course of lung emptying during
exhalation can be described with an exponential function:

\begin{equation}
V(t) = V_0 \times e^{-t/\tau} + V_{\rm exp},
\end{equation}
where
\begin{itemize}[nosep]
\item $V(t) =$
    remaining lung volume at any time point during exhalation,
\item $V_0 = $
    lung volume at the beginning of exhalation,
\item $t =$
     time elapsed from the onset of exhalation,
\item $V_{\rm exp} = $
     lung volume remaining at end-exhalation and
\item $\tau = $
     expiratory time constant.
\end{itemize}

The time constant ($\tau$) of this exponential function is proportional to the time
required for complete exhalation. By approximation, $\tau$ describes the time
required for exhaling $1-1/e \approx 63$\% of tidal volume, with $3\tau$ leading to almost
complete exhalation of at least 95\% of tidal volume. It can be obtained by
multiplying \Crs with expiratory air flow resistance (\Re). Increased \Re is a
distinctive feature of obstructive lung diseases like Asthma and COPD, leading
to high values of $\tau$ and delayed lung emptying. 

In tracheally intubated mechanically ventilated patients with COPD, the global
value of $\tau$ can be obtained from the ratio of exhaled tidal volume to expiratory
air flow (\cite{Lourens2000Expiratory}). This approach requires accurate measurements
of air flow and volume, that can easily be obtained in tracheally intubated
patients but are usually not feasible in patients undergoing non-invasive
ventilation or unassisted spontaneous breathing. Furthermore, COPD and Asthma
may exhibit regional differences in time constant, that cannot be assessed with
a global measurement approach. EIT allows regional assessment of time
constants, that should technically be feasible in patients undergoing
non-invasive ventilation or unassisted spontaneous breathing. For this purpose,
a curve characterized by the exponential function 
$V(t) = V_0 \times e^{-t/\tau} + V_{\rm exp}$
is fit to the expiratory part of the regional impedance-time-curve starting at
75\% of tidal volume. Patients with COPD exhibit high levels of regional $\tau$ that
can be modified by application of PEEP and, presumably, other therapeutic
measures like inhalation of bronchodilators (\cite{Karagiannidis2018Regional}).


\section{%
Comparison of different approaches for optimizing mechanical ventilation with EIT
}

In this section, the various approaches that have been proposed for
optimization of mechanical ventilation with EIT are compared. The advantages
and disadvantages of each are indicated and a recommendation given.
\COMMENT{Really nice section. Thanks for taking my advice!! -AA}

\subsection{%
Gravity-dependent ventilation distribution 
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Assess ventilation distribution along the gravitational axis by
analyzing ventral and dorsal fraction of ventilation (depending on patient
position) or geometrical center of ventilation at the bedside
 (cf.~\figref{fig:ch9:1VentDistribution}\ref{fig:ch9:2CoV})



 \item Aim for ``normal'' values (dorsal fraction of ventilation 50-60\%; center
of ventilation ~55\%)

 \item If ventilation of dependent lung areas (e.g., dorsal fraction of
ventilation in supine position, ventral fraction of ventilation in prone
position) is lower than ventilation of non-dependent lung areas, increase PEEP

 \item If ventilation of dependent lung areas is above normal, decrease PEEP or VT
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Easily applicable at the bedside, straightforward
 \item Feasible during assisted mechanical ventilation
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Normal values of ventral / dorsal fraction of ventilation and center of ventilation have not been established in a large cohort and may depend on patient demographics (height, weight, sex)
 \item A shift in ventilation distribution from non-dependent to dependent lung areas may be due to recruitment of dependent lung or overdistension of non-dependent; this cannot be differentiated with this approach.
 \item No clinical outcome data available
\end{itemize}

\ADVDISPRAC{Recommendation:} 
Ventilation distribution along the gravitational axis may be used to generate a first hypothesis on whether PEEP should be increased or decreased. More accurate assessments may be necessary to confirm this hypothesis. 

\subsection{%
Global inhomogeneity index and coefficient of variation
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Calculate GI index or CV at the bedside, increase or decrease PEEP until both parameters are minimized
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Easy to understand at first glance
 \item May be feasible during assisted mechanical ventilation 
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Both parameters are highly dependent on lung area investigated; if used with functional regions of interest results may be misleading; other approaches like calculation of GI index and CV from anatomical lung areas have not been validated
 \item No clinical outcome data available
\end{itemize}

\ADVDISPRAC{Recommendation:}
At present, do not use GI index and CV for optimization of mechanical ventilation in a clinical setting

\subsection{%
Intratidal ventilation inhomogeneity
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Perform slow inflation maneuvers (``low-flow pressure-volume loop'') at
different PEEP levels, starting each maneuver with the respective PEEP value
and inflating the lung at an air flow of $\le 12$\,l/min up to an inspired volume of
12\,ml/kg. 
 \item Calculate SDRVD from slow inflation maneuver performed at each PEEP level; select PEEP level with lowest value of SDRVD (cf.~\figref{fig:ch9:3SDRVD})
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Helps to identify the PEEP level corresponding to minimized alveolar cycling 
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Provides no information on regional overdistension
 \item Slow inflation maneuvers must be performed correctly and EIT analysis must be restricted to these maneuvers to yield valid results
 \item Not feasible during assisted mechanical ventilation or in the presence of spontaneous breathing efforts
 \item No clinical outcome data available
\end{itemize}

\ADVDISPRAC{Recommendation:}
May be used to compare different PEEP levels in terms of alveolar cycling.
If used, it should be complemented by measures of overdistension. 


\subsection{%
Quantification of alveolar overdistension and collapse during a decremental PEEP trial (``Costa-Approach'')
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Perform decremental PEEP trial starting at the highest clinically
acceptable PEEP level (e.g., 24\,mbar in ARDS patients or 20\,mbar in non-ARDS
patients). Observe hemodynamic stability, adjust vasopressors / fluid therapy
as necessary.
 \item Reduce PEEP by 2 or 3\,mbar every minute, until the lowest clinically
acceptable PEEP level is reached (usually around 5 mbar)
 \item Analyze changes in pixel compliance according to the ``Costa Approach'',
interpreting compliance loss towards higher PEEP levels as overdistension and
compliance loss towards lower PEEP levels as collapse
 \item Select PEEP level with ``best compromise'' between overdistension and collapse
      (cf.~\figref{fig:ch9:4Costa})
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Allows simultaneous detection of alveolar overdistension and collapse for every PEEP level
 \item Yields ``best compromise'' PEEP level 
 \item Has been successfully applied in multiple clinical studies
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Requires PEEP trial starting at very high levels (that may be associated
overdistension while applied) and continuing to very low PEEP levels (that may
lead to alveolar collapse and atelectasis formation)
 \item Should not be repeated too often because of possible side-effects
 \item Not feasible during assisted mechanical ventilation or in the presence
of spontaneous breathing efforts
 \item Limited clinical outcome data
\end{itemize}

\ADVDISPRAC{Recommendation:}
Method of choice for initial PEEP setting with EIT in patients with early ARDS
on controlled mechanical ventilation. After an initial PEEP level is found
with this method, other methods for continuous monitoring of ventilation
distribution should be preferred. Repeated decremental PEEP trials should be
avoided. 


\subsection{%
Assessment of changes in regional compliance with different VT or PEEP level
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Perform variation in VT, for example by halving inspiratory pressure
difference during pressure-controlled ventilation or set VT during
volume-controlled ventilation for a few breaths only
 \item Assess changes in regional \Crs with lower VT
 \item Decreases in regional \Crs with lower VT are interpreted as tidal
recruitment and PEEP should be elevated by 2-3\,mbar, then maneuver should be
repeated
 \item Increases in regional \Crs with lower VT are interpreted as
overdistension and VT should be reduced to a lower value, if possible. If no
reduction in VT is possible, consider PEEP reduction
       (cf.~\figref{fig:ch9:5VT}).
\end{itemize}

\begin{figure}[h]\centering
\includegraphics[width=0.8\columnwidth]
   {\CURRCHAP/fig-5VT-Var/Figure5_VT-Var_PEEP10.pdf}
\FIGCAPTION{fig:ch9:5VT}{\CURRCHAP/fig-5VT-Var}
\end{figure}



\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item May allow simultaneous detection of alveolar overdistension and alveolar
cycling for every PEEP level
 \item Individual adjustment of PEEP and VT
 \item No need for decremental PEEP trial
 \item Can be used during ongoing mechanical ventilation to decide whether PEEP / VT should be increased or decreased
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Not feasible during assisted mechanical ventilation or in the presence of spontaneous breathing efforts
 \item Clinical experience still limited
 \item No clinical outcome data available
\end{itemize}

\ADVDISPRAC{Recommendation:}
Perform initial PEEP adjustment according to ``Costa Approach'', then perform
variation in VT whenever a new assessment is required. Do not use in patients
with spontaneous breathing efforts! 


\subsection{
Poorly ventilated lung areas (``Silent Spaces'')  
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Assess dependent and non-dependent Slient Spaces at clinically selected PEEP
 \item If dependent $>$ non-dependent Slient Spaces, increase PEEP
 \item If non-dependent $>$ dependent Slient Spaces, decrease PEEP
 \item Assess changes in SilentSpaces after PEEP-step (cf.~\figref{fig:ch9:2CoV})
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Straightforward
 \item Can be used in patients with spontaneous breathing efforts
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item Relies heavily on presumed ``lung contour''; may be inaccurate if a
patient's anatomy is different from the selected model
 \item Non-dependent Silent Spaces may underestimate the actual degree of overdistension
 \item No clinical outcome data available 
\end{itemize}

\ADVDISPRAC{Recommendation:}
May be helpful for assessing changes in lung recruitment. Should be complemented by other measures for excluding overdistension during PEEP titration. 

\subsection{
Analyzing changes in end-expiratory lung impedance 
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item Perform recruitment maneuver (could be sustained inflation maneuver or
``staircase'' recruitment maneuver with stepwise PEEP increments)
 \item Assess time course of end-expiratory lung impedance after recruitment
maneuver while ventilating the patient with constant PEEP
 \item If progressive decrease in end-expiratory lung impedance with constant
    PEEP is observed, this is interpreted as alveolar derecruitment and a higher
    PEEP level should be selected to stabilize end-expiratory lung volume. Repeat
    analysis at higher PEEP level.
 (cf.~\figref{fig:ch9:6dEELI}A)
 \item If progressive increase in end-expiratory lung impedance is observed,
this is interpreted as ongoing lung recruitment. The set PEEP level should be
maintained.
 \item If no increase or decrease in end-expiratory lung impedance is observed,
    this is interpreted as stable end-expiratory lung volume. If sufficient lung
    recruitment has been achieved prior to this analysis, the set PEEP level should
    be maintained
     (cf.~\figref{fig:ch9:6dEELI}B)
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Easy to perform and relatively straightforward
 \item Changes in end-expiratory lung impedance with constant PEEP are
sensitive parameters for detection of recruitment and derecruitment
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item No assessment of overdistension
 \item Highly susceptible to artifacts and misinterpretation caused by patient
movement, fluid therapy, pulsating mattress etc.
 \item No clinical outcome data available
\end{itemize}

\ADVDISPRAC{Recommendation:}
Under very controlled conditions (no patient movement, no intravenous fluid
administration, no pulsating mattress, no other sources of interference,
constant PEEP level), changes in end-expiratory lung impedance may be a
sensitive marker for lung recruitment and derecruitment. Should be analyzed
with caution and must always be complemented by other analyses (e.g., regional
compliance changes) for excluding overdistension.

\subsection{%
Regional expiratory time constants 
}

\ADVDISPRAC{Practical approach:}
\begin{itemize}[nosep]
 \item In patients with severe expiratory airflow obstruction, assess regional
values of $\tau$ before and after therapeutic measures like adjustment of PEEP or
inhalation of bronchodilators
 \item If therapeutic measure is associated with a decrease in average regional
$\tau$, this can be interpreted as potentially beneficial (less severe airflow
obstruction)
 \item If therapeutic measure is associated with no decrease or even an increase
in average regional $\tau$, this is interpreted as not beneficial (e.g., increased
expiratory airflow obstruction)
\end{itemize}

\ADVDISPRAC{Advantages:}
\begin{itemize}[nosep]
 \item Allows assessment of expiratory airflow obstruction in patients with COPD and Asthma
 \item Requires no specific ventilatory mode or maneuver
 \item Theoretically feasible in assisted mechanical ventilation or unassisted spontaneous breathing
\end{itemize}

\ADVDISPRAC{Disadvantages:}
\begin{itemize}[nosep]
 \item A decrease in regional $\tau$ may not only be caused by a decrease in \Re, but also by decrease in regional \Crs (which might be caused by alveolar overdistension)
 \item For subjects undergoing assisted mechanical ventilation or unassisted spontaneous breathing, this approach has not yet been validated
 \item No outcome studies 
\end{itemize}

\ADVDISPRAC{Recommendation:}
Regional $\tau$ in patients with obstructive lung diseases offers valuable bedside
information on the emptying characteristics of the patient’s lungs. However,
the results should only be interpreted by trained specialists with a deep
understanding of respiratory physiology.


\section{%
Conclusion
}

Ever since its availability for clinical use, the interest in chest EIT has
been growing rapidly. Current devices allow EIT examinations to be carried out
for prolonged periods of time and present a variety of potentially useful
results at the bedside. Still, the correct application of EIT and the
interpretation of its results remain challenging and time consuming. Factors
like the high dependency of EIT results on the position of the electrode plane
(\cite{Karsten2016Influence}) and the numerous sources of interference in the
electromagnetically ``noisy'' environment of an intensive care unit
(\cite{Frerichs2011Patient}) complicate the use of EIT in clinical practice. 

The most important challenge to a wider application of EIT in clinical
practice, however, is the correct interpretation of EIT measures and their
potential clinical consequences. In this chapter, we intended to provide
background information on some of the most widely used EIT parameters as well
as recommendations on their use in clinical practice. Nevertheless, prospective
optimization of ventilator settings with EIT should only be performed by
experienced practitioners with both expertise in mechanical ventilation and a
sufficient understanding of EIT technology and its limitations. 

Future improvements in EIT hardware and bedside evaluation software should
focus on faster and easier applicability in clinical practice and increase the
robustness of EIT results with respect to confounding factors like patient
movement and fluid administration. Furthermore, the current dependency of
results on the EIT electrode plane should be reduced, perhaps by implementing
three-dimensional EIT that might correct for this confounder by automatically
analyzing multiple image planes simultaneously. Decision support tools might
facilitate interpreting the additional information provided by EIT given the
limited amount of time available for decision-making in modern intensive care
medicine. These and other developments may foster the clinical application of
EIT by users with variable degrees of training and expertise. 




