\chapter{%
Veterinary applications of EIT
}
\chapterauthor{
Martina Mosing, Yves Moens
}


\begin{center}\begin{tabular}{lp{10cm}}
 & Task List Chapter \\\hline
   \checkmark& Send to author for comments
\\ --        & Confirm rights for figures
\\ --        & Implement author changes
\\ --        & Confirm affiliations
\end{tabular}\end{center}

\section{%
Introduction to Thoracic EIT in Veterinary Applications
}

The scientific development of thoracic EIT in humans relied on translational research performed on experimental animals, especially swine. However, veterinary anaesthetists recognized EIT‘s potential for use in other species such as horses, which are too large for conventional lung imaging techniques. Active research in this field began just over a decade ago. Today, research and clinical utilization of EIT in veterinary medicine is still in its infancy, and veterinary-specific literature is scant, in comparison to human medicine. 


\subsection{%
Creation of Finite Element Models for Animals }

Anatomically-correct reconstruction of electrical impedance tomography (EIT)
images in different species necessitates the use of finite element (FE) models.
The routine method to calculate an average FE model is by using helical
computed tomography (CT) scans during inspiratory hold. From the CT images
obtained at the electrode plane of the EIT belt, the heart, lungs and thoracic
contours are segmented. Segmented image files from several animals are then
used to create an average contour and finally the corresponding FE model for
that species. These FE models are then used to guide placement of the
electrodes and create the final mesh for image reconstruction [Waldmann, 2016
\#64].

Species-specific FE models have been created at the height of the 6th
intercostal space at half width of the thorax for beagle dogs and calves
respectively, as well as for pigs and ponies [Waldmann, 2016 \#65]. 

In larger species that do not fit in CT scanners, alternative methods are
required to create FE models. One method is to construct models from
photographs taken of thoracic sections of anatomically dissected specimens.
This obtains the essential topographic anatomy, including the dimensions of the
lungs, heart and body-wall contour necessary to create the FE model. This has
been performed in a horse and a rhinoceros [Brabant, 2018 \#64]. Another method
used in live horses is to use gypsum to fabricate casts of the thoracic
contours. These shapes are subsequently digitized and averaged including
assumed organ location based on published anatomical studies [Ambrisko, 2016
\#15]. 
These techniques are illustrated in \figref{fig:Animal-FEM}.


\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{\CURRCHAP/fig-FEM-creation/image1.png}
\FIGCAPTION{fig:Animal-FEM}{\CURRCHAP/fig-FEM-creation}
\end{figure}

\section{%
Translational Research in Animals
}

Similar to other new medical technologies, new EIT algorithms that show promise
are validated using animal models before they are released as clinical
applications in human medicine.

\subsection{Pig}

The pig is the most commonly used species for translational studies in EIT
research as they have a similar thoracic circumference for EIT belts to humans
(compared to rabbits and rodents), their topographic anatomy is relatively
similar to humans (compared to ruminants such as sheep), they grow relatively
quickly (compared to dogs) and have relatively lower ethical concerns (compared
to primates). As of this writing, more than 150 papers have been
\COMMENT{Is it worth updating this number? -AA}
published on EIT in different pig models (Pub Med search (electrical impedance
tomography) AND (pig OR swine OR porcine)). The majority of reports using pigs
evaluate changes in EIT algorithms in acute respiratory distress syndrome
(ARDS) after induced lung injury. When interpreting EIT ventilation data
obtained from pig studies, the anatomical and physiological differences between
the human and porcine lung have to be considered; pleura and the
non-respiratory bronchioles show grossly the same anatomical features, but the
interlobular and segmental connective tissue, and the alveolarised respiratory
bronchioles, are different between the two species [Peake, 2015 \#67]. One
important difference is that in humans, the interlobular connective tissue only
partially surrounds the lung lobules whereas in pigs this tissue forms complete
interlobular septa. This extensive inter-lobular connective tissue prevents
collateral ventilation between adjacent lobules in pigs [Van Allen, 1931 \#68].
This difference must be acknowledged when extrapolating EIT data after specific
ventilatory interventions from pigs to humans as it is collateral ventilation
which partially prevents the development of lung collapse after distal airway
obstruction [Robinson, 1982 \#69]. This makes the diseased human lungs less
susceptible to atelectasis compared to pig lungs. These physiologic and
anatomical differences between species might result in unknown effects on
pulmonary function in response to different external insults, which needs to be
taken into consideration when using EIT as a functional lung imaging technique. 

Pig models are also often used in cardiovascular studies in translational
research although pronounced differences in the anatomy of pulmonary vessels,
muscular layers of arteries and location of pulmonary veins exist between pigs
and humans. The latter should be considered when results from EIT studies using
pig models are used for EIT interpretation in humans [Kay, 2015 \#70]. In
particular, the pronounced hypoxic pulmonary vasoconstrictor response present
in pigs [Robinson, 1982 \#69] requires caution when the results of EIT lung
perfusion studies are extrapolated to humans. 

\subsection{Dog}

From an anatomical point of view dogs are the opposite of pigs. They have an
excellent collateral ventilation and minimal hypoxic pulmonary vasoconstriction
[Peake, 2015 \#67]. This at least makes dog lungs functionally more comparable
with human lungs [Woolcock, 1971 \#74]. This also makes dog models more
reliable in translational research to monitor specific ventilatory
interventions using EIT. Dogs are not used extensively anymore in translational
research; however, partly because they are more difficult to breed and house
for research purposes compared to pigs, and partly due to ethical concerns.
Some early EIT papers, which describe the use of dog models and a 16 electrode
EIT system, showed that EIT can be used to estimate changes in tidal volume,
lung inflation at the end of expiration and liquid content in the lung [Adler,
1997 \#73] [Adler, 1998 \#72]. 
\COMMENT{There are also early publications from Newell on dogs -AA}

Ambrisko and co-workers used a dog model to evaluate the influence of
anaesthesia on the distribution of ventilation [Ambrisko, 2017 \#75]. They were
able to show that anaesthesia, but not recumbency, affected the centre of
ventilation and inhomogeneity factor in these dogs suggesting that this may
also apply in humans.

\subsection{Lamb and sheep}

Sheep are straightforward to house, breed and handle and are therefore often
used in translational research. Lambs have become an established animal model
for preterm lung research using EIT. Sheep usually have one offspring in
comparison to pigs or dogs and construction of EIT belts for preterm lambs is
not limited by their size. Ethical concerns are also lower in lambs than in
preterm non-human primates.
\COMMENT{Is this correct? I thought sheep normally had twins. Also
mention that newborn sheep are bigger and thus a better model
of neonates -AA}

Tingay and colleagues used EIT monitoring to compare the effects of different
ventilation strategies used to inflate the lungs of premature newborn lambs
[Tingay, 2014 \#77] [Tingay, 2014 \#76] [Tingay, 2015 \#80] [Tingay, 2019
\#81]. During one experiment, EIT was used to diagnose the occurrence of a
spontaneous pneumothorax for the first time [Miedema, 2016 \#83].

The use and interpretation of thoracic EIT in adult sheep is challenging as the
rumen occupies a large volume on the left side of the body. The rumen is one
part of the multicompartmental stomach in ruminants and represents a gas filled
viscus adjacent to the diaphragm, partially covered by the ribcage. This can
interfere with the ventilation signal when the cranial part of the rumen moves
into the EIT plane. The latter is even more likely in recumbent anaesthetised
sheep as the natural escape mechanism for accumulating gas (eructation) is
disabled, causing ruminal tympany. Therefore, EIT data in sheep and other
ruminants have to be interpreted carefully. 

A sheep model has been used to investigate EIT as a monitoring tool to detect
pulmonary embolism [Nguyen, 2015 \#101]. EIT images of lung perfusion
were created by injecting different concentrations of saline solution into the
right atrium. 

\subsection{Horse}

Given what is known about equine lung physiology and anatomy, horses are
potentially the best lung model for translational research.
\COMMENT{Except for their size and cost! - AA}
 In both the human
and the horse, the separation of lung lobules is incomplete and collateral
ventilation and hypoxic pulmonary vasoconstriction are comparable [Peake, 2015
\#67]. Furthermore, after induction of anaesthesia and recumbency, horses show
reproducible small airway collapse, atelectasis and increases in venous
admixture within a short time [Nyman, 1989 \#85]. Therefore, different
ventilation strategies can be studied to re-inflate these atelectatic lung
regions without the confounding variables of inflammation and
bronchoconstriction which play a role in all other induced lung-injury animal
models.

One EIT paper evaluated the differences of 3D image reconstruction in humans
and horses [Grychtol, 2019 \#92]. In both species, the data obtained from a
2-plane belt (2$\times$16 electrodes) instead of the traditional one-plane 32
electrode belt showed anatomically plausible images. Image reconstruction of
the horse data revealed a gas-filled organ ventral to the caudal lungs,
corresponding to the anatomical position of the large dorsal colon in horses.


\section{%
Clinical Research in Animals 
}

In this section, we review the use of EIT clinical research where the
primary goal
is to improve diagnosis and treatment of the animal, rather than as a
translational vehicle for human disease.

\subsection{Horses}
Most clinical research involving EIT in animals has been performed in horses.
This research was guided by the need to understand and treat
ventilation/perfusion mismatching and the resultant venous admixture which
occurs during anaesthesia in this species [Nyman, 1989 \#85]. Until the
implementation of EIT into equine anaesthesia research, evaluation of the
pathophysiology of ventilation, and possible treatment, was difficult as
routine diagnostic imaging is very challenging (radiography, scintigraphy) or
impossible (CT or MRI) due to the size of the animal. The use of EIT in horses
has been made possible by the development of large custom-made rubber or
neoprene belts with metal electrodes attached at equal distances.

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{./\CURRCHAP/fig-Horse+EIT/image2.png}
\FIGCAPTION{fig:EIT-horse}{./\CURRCHAP/fig-Horse+EIT}
\end{figure}


\begin{enumerate}
\item {\bf Distribution of ventilation}

The distribution of ventilation has been described using EIT in standing
unsedated horses [Schramel, 2012 \#84][Mosing, 2016 \#4]. \Figref{fig:EIT-horse} shows
collection of EIT data in a standing horse. The effect of pregnancy on the
distribution of ventilation was studied in Shetland ponies before and after
parturition [Schramel, 2012 \#84]. The results confirmed that the increasing
size of the uterus influences the distribution of ventilation by shifting
ventilation towards dorsal lung areas and that this distribution abruptly
normalises after birth. 

A feasibility study to evaluate the distribution of ventilation by evaluation
of the fEIT parameters, left-to-right ratio, centre of ventilation and global inhomogeneity
index was performed in standing horses [Ambrisko, 2016 \#15]. Functional EIT
images were created using standard deviations (SD) of pixel signals and
correlation coefficients (R) of each pixel signal compared with a reference
respiratory signal. Different interventions (sighs, CO$_2$-rebreathing and
sedation) coupled to plethysmographic spirometry were used to confirm
feasibility of EIT to detect changes in ventilation. An interesting finding was
that an inverse respiratory signal was found in the most ventral dependent
region of the EIT image. This is thought to be due to the cyclic appearance of
a gas-filled abdominal organ (most likely the dorsal large colon) into the EIT
plane at the 4-5th intercostal space. This signal should not be interpreted as
being of lung and needs to be distinguished from heart origin when detailed
analysis of the thoracic EIT signal in the horse is required.

The breathing pattern of horses and the regional distribution and dynamics of
ventilation were also evaluated prior to and following recovery from general
anaesthesia in dorsal (supine) recumbency [Mosing, 2016 \#4]. To determine
regional time delays within the lungs, the inflation period of seven regions of
interest (ROIs) evenly distributed along the dorso-ventral axis of the lungs
was calculated. The regional filling time was defined as the time at which
$\Delta Z(t)$ of each region reached 50\% of its maximum impedance change during
inspiration, normalised by the global inspiratory time. The regional inflation
period was defined as the time period during which $\Delta Z(t)$ of each lung region
remained above 50\% of its maximum inspiratory impedance change, normalised by
the global breath length. After recovery of anaesthesia in dorsal recumbency a
respiratory pattern with inspiratory breath-holding was observed for six hours
after standing. During these episodes, EIT showed that ventral lung areas were
emptying while the dorsal areas were still filling, suggesting redistribution
of air from ventral into dorsal lung regions. This is considered an
auto-recruitment mechanism of lung tissue in areas which were dependent, and
likely atelectatic, during anaesthesia. 

Several studies have specifically addressed the distribution of ventilation in
horses during the anaesthetic period [Ambrisko, 2017 \#2] [Auer, 2019 \#91]
[Mosing, 2018 \#5] [Mosing, 2017 \#6] [Mosing, 2019 \#16]. 

In mechanically ventilated ponies in right lateral recumbency, the centre of
ventilation (CoV) was in the non-dependent left lung [Auer, 2019 \#91]. A shift
of CoV from the dependent to the less perfused non-dependent areas of the lungs
was also shown after initialisation of volume-controlled ventilation (CMV) in
dorsally recumbent anaesthetised horses [Mosing, 2017 \#6]. The initiation of
CMV was accompanied by an increase in silent spaces and a decrease in
ventilation in the dependent lung areas suggesting additional atelectasis
development. This may be one of the reasons why a lack of improvement in
oxygenation is frequently seen when switching from spontaneous to controlled
ventilation in anaesthetised horses. EIT has also
been used to show the effect of specific ventilation interventions; one case
report evaluated the recruitment of collapsed lung areas in the dependent lung
under EIT guidance. The changes in ventilation distribution were measured in
real time [Moens, 2014 \#10]. Regional lung compliance was calculated by
simultaneous EIT and airway pressure measurements. This case report also
discussed preliminary perfusion-related information from the EIT signal.
Changes in a ventilation/perfusion mismatch algorithm based on the EIT signal
was compared to changes in blood gas analysis and showed promise for EIT-based
evaluation of ventilatory interventions for V/Q mismatch. 

In anaesthetised horses, as in humans, recruitment manoeuvres or continuous
positive airway pressure (CPAP) can be used to reinflate collapsed alveoli or
to maintain alveolar patency, respectively – a process that can be demonstrated
and confirmed by EIT [Ambrisko, 2017 \#2] [Mosing, 2018 \#5]. As described in
the human literature, the simultaneous recording of EIT signal and airway
pressure allows the evaluation of regional lung compliance. A positive
relationship between increases in dependent lung compliance following
recruitment manoeuvres and blood oxygenation has been found [Ambrisko, 2017
\#2]. In the CPAP study, the CoV shifted to dependent parts of the lungs
indicating a redistribution of ventilation towards those dependent lung
regions, thereby improving ventilation-perfusion matching. Dependent silent
spaces were significantly smaller when CPAP was applied compared to anaesthesia
without CPAP, and non-dependent silent spaces did not increase demonstrating
that CPAP leads to a decrease in atelectasis without causing overdistension
[Mosing, 2018 \#5]. 


\item {\bf Tidal volume}

The routine measurement of tidal volume in anaesthetised horses is not
performed due to a lack of practical and affordable equipment for tidal volumes
of up to 20 litres. In some situations, spirometric information would be highly
desirable because capnography results can falsely suggest adequate ventilation
despite the presence of pronounced ventilation/perfusion mismatch [Mosing, 2018
\#86]. In one study in mechanically ventilated anaesthetised horses, a very
close relationship between measured tidal volume and impedance change was found
for a wide range of tidal volumes (4 – 10 L per breath). This relationship was
further improved when total impedance change in the entire image was calculated
compared to impedance changes within lung ROIs. The authors concluded that
during breaths with high tidal volumes. the lungs become overinflated and extend
over the ROI borders leading to a loss of correct ``volume'' information compared
to the FE model used [Mosing, 2019 \#16]. It may be possible to use a conversion
factor to translate impedance changes measured by EIT into absolute tidal
volume values, but future studies are required.
\COMMENT{We now have papers on this. Add?? --AA}

\item {\bf Airflow}

Equine asthma, also known as heaves, is a chronic performance-limiting disease
which affects horses of all ages and breeds. The disease is characterised by
bronchoconstriction, mucus production and smooth muscle remodelling, however
the clinical signs are nonspecific and can be subtle, which makes diagnostic
evaluation challenging, particularly when examining horses in the field. This
is also true for monitoring the effectiveness of treatment in severe cases
suffering from respiratory distress.

Global airflow signals in the lungs can be generated by calculating a smoothed first
derivative of the EIT volume signal. This has been used to demonstrate global
and regional ventilation characteristics related to changes in airway diameter
in people [Ngo, 2018 \#88]. 

Changes in global and regional peak airflow calculated from the EIT signals
have been compared with a validated method (flowmetric plethysmography) in
standing horses before and after histamine-induced bronchoconstriction
[Secombe, 2019 \#89]. The expiratory peak flow calculated with EIT changed in
accordance with the flowmetric plethysmography variable, particularly in the
ventral lung regions. 

In another study in horses, peak flows and flow-volume curves were generated
from the EIT signal before and after inhalation of nebulized histamine. When
signs of airflow obstruction appeared, bronchodilation was obtained with
aerosolised salbutamol [Secombe, 2019 \#90]. This study showed that EIT can
verify histamine-induced airflow changes and also subsequent reversal of these
changes by using a bronchodilator. EIT therefore has potential as an additional
method for pulmonary function testing and as a non-invasive monitoring tool to
guide therapy in horses with equine asthma. 

The same concept of monitoring airflow with EIT was used to evaluate changes in
healthy and asthmatic horses before and after exercise, after return to
baseline respiratory rates [Herteman, 2019 \#124]. Global and regional EIT gas
flow changes were seen after exercise in horses suffering from equine asthma.
More pronounced flow changes were noted in the ventral dependent areas of
the lung, which is in accordance to the observed changes in the ventral lung
areas after histamine-induced bronchoconstriction.
\end{enumerate}

\COMMENT{Can we add some concluding remarks about horses here -AA}

\subsection{Dog}

\COMMENT{Can you briefly explain what the clinical value of EIT for
dogs is? --AA}
There are relatively few clinical research studies assessing EIT measurements
in dogs. This can be primarily related to the fact that good image quality is
more difficult to achieve. Dogs have longer fur than horses and therefore need
to be clipped when flat EIT electrodes are used. Furthermore, their skin seems
to have a higher resistance to the current (possibly due to the lack of sweat
glands), making it difficult to collect good EIT raw data. Dog skin and
subcutis is highly mobile over the muscles which makes it difficult to have a
stable belt position and heart-related impedance changes are very prominent in
the thoracic plane which provides maximum lung area (personal experience). The
optimal EIT belt position for clinical use has been determined based on
radiographic and CT images using ``fake'' EIT belts [Rocchi, 2014 \#93],
and shown in \figref{fig:Dog-Xray}. The
sternum was used as an external landmark as this is palpable even in obese
dogs.

\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{\CURRCHAP/fig-Dog-Xray/image3.png}
\FIGCAPTION{fig:Dog-Xray}{\CURRCHAP/fig-Dog-Xray}
\end{figure}


Two studies have been conducted in anaesthetised dogs evaluating the
distribution of ventilation using a 16 electrode EIT system (EIT Evaluation Kit
\COMMENT{Please correct \ldots I don't know of a kit 3n}
3n for Regional Lung Monitoring; Dräger Medical AG). One study evaluated the
effects of different PEEP levels in dogs in sternal (prone) position [Gloning,
2017 \#95], while the other study looked at the change in the distribution of
ventilation before and after a recruitment manoeuvre using two different tidal
volumes [Ambrosio, 2017 \#94]. In both studies, EIT was able to verify a change
in the CoV and ventilation in four regions of interest ordered from dependent
to non-dependent. Results were comparable with changes observed in CT images
and other measured functional ventilation variables, confirming the validity of
EIT to monitor distribution of ventilation in anaesthetised dogs.

Two other studies used EIT to monitor ventilation in sedated dogs. In both
studies, a 32 electrode EIT belt was used (BBVet, Sentec AG). In one study the
effect of the application of three CPAP interfaces on end-expiratory lung
impedance (EELI) was evaluated as a surrogate for functional residual capacity,
which is expected to increase under CPAP ventilation. Changes in thoracic
impedance change ($\Delta Z$) were used as estimates of changes in tidal volume [Meira, 2018
\#96]. Whereas all devices induced a significant increase in EELI when CPAP was
applied, no difference between the interfaces was found in EIT variables.
\COMMENT{I'm not clear on what ``devices'' and ``interfaces'' mean here -AA}
Another study used EIT to monitor breathing pattern and changes in tidal
volume following the application of two sedatives (dexmedetomidine and
medetomidine). Results show that both drugs decrease respiratory rate and
increase tidal volume ($\Delta Z_{TV}$) suggesting that minute ventilation remains
essentially unchanged [Pleyers, 2019 \#11].

\subsection{Rhinoceros}

Rhinoceroses are the largest mammals in which EIT signal collection has been
performed, using custom made electrode belts. These animals experience a
profound negative impact on gas exchange while being anaesthetised which leads
to severe hypoxaemia and hypercapnia [Radcliffe, 2014 \#97]. The gas exchange
is worse in lateral compared to sternal recumbency and it was speculated that
changes in pulmonary dead space were the major contributor to the impairment
seen in lateral recumbency. 
\COMMENT{These studies were motivated by the need to ... during removal of the horn ...}

EIT has also been successfully used in anaesthetised rhinoceroses in lateral
and sternal recumbency [Mosing, 2017 \#98]. A large difference in the
distribution of ventilation between the two lungs was observed. In lateral
recumbency, the non-dependent lung contributed 80\% of the total $\Delta Z$, while the
dependent lung was only minimally ventilated. In sternal recumbency the
difference in ventilation between the two lungs was only 10\%. These findings
show that the impairment in gas exchange in anaesthetised lateral recumbent
rhinos is more likely due to marked ventilation-perfusion mismatch, rather than
an increase in pulmonary dead space, as previously suggested.


\section{
Clinical Applications in Animals 
}

To date, EIT has not been used extensively in clinical situations as displayed
variables do not give the necessary information at the bedside to change case
management. Reports on the use of EIT in clinical cases are mostly anecdotal.
However, some case reports and case studies can be found in the veterinary
literature. 

\begin{enumerate}
\item {\bf Horse}

A case study with six horses endorses the potential usefulness of EIT as a
diagnostic imaging technique in large animal species [Herteman, 2019 \#125].
Two healthy horses, one with exercise-induced pulmonary hemorrhage (EIPH), two
with pleuropneumonia and one with cardiogenic pulmonary oedema (CPE) were
included. EIT was recorded in these standing non-sedated horses over 10 breath
cycles and the distribution of ventilation was evaluated (CoV and silent
spaces). In the horses with lung disease, a shift of CoV towards the
non-dependent lung fields/segments and an increase in dependent silent spaces
was demonstrated, whereas these parameters remained unchanged in healthy
horses. EIT was thus able to detect changes in the distribution of ventilation
as well as the presence of poorly ventilated lung units in naturally occurring
pulmonary pathology in awake horses.

EIT data from a horse suffering from pulmonary oedema due to cardiac disease
were collected over several months [Sacks et al, 2019]. A distinct dorsal shift
of the ventilation with loss of ventilated lung area was observed in the fluid
filled lungs depending on the clinical severity of pulmonary oedema at each
measurement point. The EIT findings were considered compatible with the
severity of the clinical signs and a distinct difference in the distribution of
ventilation was found when compared to a cohort of healthy horses.

\item {\bf Orangutan}

A case report describing the clinical use of EIT in an orangutan with recurrent
airway disease undergoing diagnostic evaluation demonstrated how EIT can help
at the bedside to detect ventilatory issues quickly and allow goal-directed
treatment [Mosing, 2017 \#123]. A belt designed for humans (Sentec,
Switzerland) was placed directly after induction of anaesthesia to continuously
monitor ventilation during planned transport between different locations within
the hospital (radiology, CT imaging, and bronchoscopy room). Directly after
endotracheal intubation one-lung intubation and ventilation became apparent on
the EIT. The endotracheal tube was withdrawn until both lungs were visibly
ventilated on the EIT screen. Nevertheless, one lung continued to show reduced
regional stretch and large silent space which was subsequently verified as a
large pathologic process (abscess) in the lung on CT. Subsequently, EIT
revealed increased silent spaces in the peripheral lung area after bronchoscopy
indicative of excess residual lavage fluid within the lung tissue. Weaning from
the ventilator was guided by improvement of the EIT image. In this case EIT
played a key role in safe anaesthesia monitoring and non-invasive diagnostics.

\end{enumerate}


\section{%
Future of EIT in veterinary applications
}
As EIT becomes established as a useful non-invasive and affordable method for
dynamic general and regional lung function examination in the veterinary field,
this new technology is expected to be increasingly used for a broader range of
indications, conditions and animal species over the coming years. 

\COMMENT{Can we add something about what the key excitment is?
 What is the most promising few applications in the near future -AA}

The authors of this book chapter have collected data from cows, sheep, foals
and birds evaluating anaesthesia- and ventilatory-related variables, as well as
data evaluating lung disease in cattle.  

Furthermore, new applications are currently being investigated. Preliminary
data on the evaluation of heart rate based on thoracic EIT measurements look
very promising. The option of heart rate measurement in addition to the
ventilation signal would further improve the clinical usefulness of EIT in
veterinary applications.


