\documentclass[12pt,a4paper,oneside]{book}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{bm}
\usepackage[margin=1in]{geometry}
\usepackage{mdframed}
\usepackage{graphicx}
\usepackage{float}
\usepackage{enumitem}
\usepackage[margin=10pt,small,bf,tableposition=top,figureposition=bottom,skip=2pt]{caption}
\usepackage[section]{placeins} %figures stay in subsection
\usepackage{placeins}
\usepackage[normalem]{ulem}
\let\Oldsection\section       \renewcommand{\section}{\FloatBarrier\Oldsection}
\let\Oldsubsection\subsection \renewcommand{\subsection}{\FloatBarrier\Oldsubsection}

\usepackage[colorlinks,allcolors=blue]{hyperref}

\usetikzlibrary{tikzmark}
\usetikzlibrary{positioning}
%\newcommand\tikzmark[2]{%
%\tikz[remember picture,baseline] \node[inner sep=2pt,outer sep=0] (#1){#2};%
%}
\raggedbottom % Avoid orhaned headings
\clubpenalty=1000

\newcommand{\COMMENT}[1]{\vspace{5mm}{\color{blue!50!black}\noindent %~\\
\framebox{ \parbox{15cm}{ #1 }} }}

\newcommand\link[2]{%
\begin{tikzpicture}[overlay,remember picture, shorten >=-3pt]
\node [above left=-0.5mm and 5mm of pic cs:#1] (TMPNODEA) {};
\node [above left=-0.5mm and 1mm of pic cs:#1] (TMPNODEB) {};
\node [above left=-0.5mm and 10mm of pic cs:#2] (TMPNODEC) {};
\node [above left=-0.5mm and 10mm of pic cs:#2] (TMPNODED) {};
\draw[thick, ->] (TMPNODEA) -- (TMPNODEB.center) -- (TMPNODEC.center) -- (TMPNODED) ;
%\draw[thick, ->] (TMPNODEA) -- (pic cs:#1) -- (TMPNODEB);
\end{tikzpicture}
}

\newcommand{\im}{\ensuremath{\textbf{i}}}
\newcommand{\figref}[1]{figure~\ref{#1}}
\newcommand{\Figref}[1]{Figure~\ref{#1}}
\newcommand{\secref}[1]{section~\ref{#1}}
\newcommand{\eqnref}[1]{(eqn~\ref{#1})}
\newcommand{\Eqnref}[1]{(Eqn~\ref{#1})}
\newcommand{\JV}{\vec{J}}
\newcommand{\EV}{\vec{E}}
\newcommand{\BV}{\vec{B}}
\newcommand{\xV}{\vec{x}}
\newcommand{\xB}{\mathbf{x}}
\newcommand{\XB}{\mathbf{X}}
\newcommand{\xH}{\hat{\mathbf{x}}}
\newcommand{\xT}{\tilde{\mathbf{x}}}
\newcommand{\yB}{\mathbf{y}}
\newcommand{\yT}{\tilde{\mathbf{y}}}
\newcommand{\vB}{\mathbf{v}}
\newcommand{\DB}{\mathbf{D}}
\newcommand{\IB}{\mathbf{I}}
\newcommand{\MB}{\mathbf{M}}
\newcommand{\RB}{\mathbf{R}}
\newcommand{\PB}{\mathbf{P}}
\newcommand{\QB}{\mathbf{Q}}
\newcommand{\WB}{\mathbf{W}}
\newcommand{\VB}{\mathbf{V}}
\newcommand{\JB}{\mathbf{J}}
\newcommand{\sG}{\bm{\sigma}}
\newcommand{\SG}{\bm{\Sigma}}
\newcommand{\Ew}{\mathop{\rm E}_w}

\newcommand\CHAPPROP[5]{
\chapter{\em #1}
\begin{center}
\begin{tabular}{lp{10cm}}
\hline
Part / Chapter:& \thepart.\thechapter
\\
Title:& #1
\\
Authors:& #2
\\
Topics:& #3
\\
Status:& #4
\\
Pages (suggested):& #5
\\ \hline
\end{tabular}
\end{center}
}


\begin{document}
\title{Electrical Impedance Tomography:
       Methods, History and Applications, 2$^{\rm nd}$ Ed}
\author{Andy Adler and David S. Holder}
\date{}
\maketitle

\chapter*{Book outline, proposal and guidance}

Documents:
\begin{itemize}
\item Book proposal: \href{http://www.sce.carleton.ca/~adler/EITv2/proposal/EITv2proposal.pdf}
                                 {www.sce.carleton.ca/~adler/EITv2/proposal/EITv2proposal.pdf}
\item Outline  \href{http://www.sce.carleton.ca/~adler/EITv2/outline/EITv2outline.pdf}
                                                    {www.sce.carleton.ca/~adler/EITv2/outline/EITv2outline.pdf}
\\ (last updated \verb$Date:: 2019-03-13$)
\end{itemize}

\section*{Proposal}

We proposed a restructured and expanded book, (see \figref{fig:prev_curr}),
 with the following
key contributions
\begin{itemize}[nosep]
\item New sections on ``Image interpretation'' and ``D-bar image reconstruction''
\item New appendices on ``Data and Software'', ``Introduction to lung physiology''
\item A restructuring of the technical material around a
      ``EIT workflow'' (\figref{fig:secEIT-overview}).
\item In each technical section, numerous short ($\frac{1}{2}$--1 page ``Questions'')
      are answered. Each answers applied questions which have commonly been posed,
      in our experience.
      (These questions will supplement the exposition in the regular text).
\item Reproducible data and software. To the extent possible, all illustrations
      will be accompanied by the software to repeat the analysis. For examples
      (see \figref{fig:EITbaby}), we will endeavour to make raw data available.
\end{itemize}


\begin{figure}
\def\LMARG{nosep,leftmargin=1cm}
\centering
\begin{mdframed}[innerleftmargin=20,innerrightmargin=0]
\begin{minipage}[t]{0.44\textwidth}\small
\begin{center}
1$^{\rm st}$ Edition
\end{center}
\begin{enumerate}[nosep,leftmargin=0cm]\setcounter{enumi}{0}
\item[$\Rightarrow$]
Introduction
\item
Algorithms
\begin{itemize}[nosep,leftmargin=5mm]
\item The Reconstruction Problem\hfill\tikzmark{ba}{~}
\end{itemize}
\item
Hardware
\begin{itemize}[nosep,leftmargin=5mm]
\item EIT Instrumentation \hfill\tikzmark{ca}{~}
\end{itemize}
\item
Applications
\begin{itemize}[nosep,leftmargin=5mm]
\item Imaging of the Thorax by EIT%
      \hfill\tikzmark{da}
\item EIT of Brain Function%
      \hfill\tikzmark{db}
\item Breast Cancer Screening with EIT%
      \hfill\tikzmark{dc}
\item Applications of EIT in the Gastrointestinal Tract (GIT)%
      \hfill\tikzmark{dd}
\item Other Clinical Applications of EIT%
      \hfill\tikzmark{de}
\end{itemize}
\item
New Directions
\begin{itemize}[nosep,leftmargin=5mm]
\item
Magnetic Induction Tomography\hfill\tikzmark{ea}{~}
\item
Magnetic Resonance Electrical Impedance Tomography\hfill\tikzmark{eb}{~}
\item
Electrical Tomography for Industrial Applications\hfill\tikzmark{ec}{~}
\end{itemize}
\item[$\Rightarrow$] History
\begin{itemize}[nosep,leftmargin=5mm]
\item
EIT: The View from Sheffield\hfill\tikzmark{ha}{~}
\item
EIT for Medical Applications at Oxford Brookes 1985-2003\hfill\tikzmark{hb}{~}
\item
The Rensselaer Experience\hfill\tikzmark{hc}{~}
\end{itemize}
\item[$\Rightarrow$] Appendices
\begin{enumerate}[label=\Alph*.,nosep,leftmargin=5mm]
\item
 Brief Introduction to Bioimpedance\hfill\tikzmark{za}{~}
\item
 Nontechnical Introduction to EIT\hfill\tikzmark{zb}{~}
\end{enumerate}
\end{enumerate}
\end{minipage}
\hspace{12mm}
\begin{minipage}[t]{0.44\textwidth}\small
\begin{center}
  2$^{\rm nd}$ Edition
\end{center}
\begin{enumerate}[label=\Roman*.,nosep,leftmargin=0cm]\setcounter{enumi}{0}
\item
Introduction
\begin{enumerate}[label=\arabic*.,nosep,leftmargin=5mm]
\item
\tikzmark{AA}{%
 Introduction to EIT (Nontechnical)}
\end{enumerate}
\item
 EIT: tissue properties to image measures
\begin{enumerate}[label=\arabic*.,resume,nosep,leftmargin=5mm]
\item \tikzmark{BA}{Tissue Properties}
\item \tikzmark{BB}{Electronics \& Hardware}
\item \tikzmark{BC}{Sensitivity}
\item \tikzmark{BD}{Image Reconstruction}
\item              {D-bar image reconstruction}
\item \tikzmark{BE}{Image Interpretation}
\end{enumerate}
\item
Applications
\begin{enumerate}[label=\arabic*.,resume,nosep,leftmargin=5mm]
\item \tikzmark{CA}{Lungs: monitoring ventilation} 
\item \tikzmark{CB}{Lung function} 
\item \tikzmark{CC}{Heart and blood flow} 
\item \tikzmark{CD}{Nervous tissue: brain and nerves} 
\item \tikzmark{CE}{Cancer \&
                    Other Clinical Applications} 
\end{enumerate}
\item Related Technologies
\begin{enumerate}[label=\arabic*.,resume,nosep,leftmargin=5mm]
\item \tikzmark{DA}{%
Magnetic Induction Tomography}
\item \tikzmark{DB}{%
Magnetic Resonance EIT}
\item
 Geophysical ERT
\item \tikzmark{EC}{%
 Process tomography}
\end{enumerate}
\item Appendices
\begin{enumerate}[label=\arabic*.,resume,nosep,leftmargin=5mm]
\item Devices, Companies and History
\begin{itemize}[nosep,leftmargin=5mm]
\item \tikzmark{EA}{%
Pioneers: Sheffield, Rensselaer, etc.}
\item
Clinical: G\"ottingen, Kiel, etc.
\item
Companies: Viasys, Dr\"ager, Dixtal/Timpel, Swisstom/SenTec:
\end{itemize}
\item
 Software and Data for EIT
\item
 Symbols and terminology
\end{enumerate}
\end{enumerate}
\end{minipage}
\end{mdframed}
\link{zb}{AA}
\link{za}{BA}
\link{ba}{BC}
\link{ba}{BD}
\link{ca}{BB}
\link{da}{CA}
\link{da}{CB}
\link{da}{CC}
\link{db}{CD}
\link{dc}{CE}
\link{dd}{CE}
\link{de}{CE}
\link{ha}{EA}
\link{hb}{EA}
\link{hc}{EA}
\link{ec}{EC}
\link{eb}{DB}
\link{ea}{DA}
\caption{\label{fig:prev_curr}%
Comparison of 1$^{\rm st}$ and 2$^{\rm nd}$ edition structure.
New sections include ``Image interpretation'', ``D-bar image reconstruction'',
and history of clinical-lung applications and companies.
Thoracic EIT is expanded into three sections.
}
\end{figure}

\section*{Guidance}

\begin{itemize}[itemsep=1.0mm]
\item {\bf Dates}
\vspace{-9mm}
\begin{flushright}
\begin{tabular}{lp{8cm}}
{\em Date} & {\em Comments}
\\ \hline
\sout{Jan 31, 2019}& \sout{Agree/Disagree to our invitation}
\\
\sout{Feb 28, 2019}& \sout{Chapter outline}
\\
\sout{May 31}
June 15, 2019&  Chapter draft --- for comment
\\
June 16--30, 2019& Assemble draft from chapters
\\
Jul 1--3, 2019& Circulate draft at EIT2019 Conf
\newline $\rightarrow$ Discussions with collaborators
\\
Aug 31, 2019&  Final Chapter
\\
2019 & Book in press
\\ \hline
\end{tabular}
\vspace{-5mm}
\end{flushright}

\item {\bf Audience}
\begin{itemize}[nosep]
\item
 We envision the primary audience is 
for researchers who are joining the EIT
field. This is primarily new graduate
students.
\end{itemize}

\item {\bf Chapter Size}
\begin{itemize}[nosep]
\item
 We have proposed an approximate
 number of pages  to help guide authors
 Pages are assumed to be single spaced, on A4 paper with 12\,pt font.
\item
 However, {\bf take the space you need} to cover the
topic.
We will argue with the publisher if size becomes
a problem.
\end{itemize}

\item {\bf Figures and Data}
\begin{itemize}[nosep]
\item 
Use lots of figures if you feel that's appropritate. We may need
to move some of them to an online suplemental material, depending
on the manuscript size.
\item 
If at all possible, we would like to provide the 
{\em data}
and
{\em software}
by which the images are generated.
\end{itemize}

\item {\bf Format}
\\
Contributions can be in latex or in msword format.
%We will make the conversion to latex in order to
%bring the document together.
Language is American English.

\item {\bf References}
\\
We will have a single references section at the 
end of the book (rather than references per
chapter). 
\begin{itemize}[nosep]
\item In latex/bibtex, we will provided a EITv2.bib file
  in the version repository.
\item If this is not convenient, put your references at
  the end of your chapter in
  word, either in the text as [NameDateSomeWords]
  (e.g. ``The first edition [Holder2005Electrical] of this
book''), or
  using a reference manager like Mendeley.
  
\item We will ask the publisher to help unify the
  references.
\end{itemize}

\item {\bf Sharing and Version Control}
\\
The latest version of the book will be
available through
\begin{itemize}[nosep]
\item \href{http://www.sce.carleton.ca/~adler/EITv2}
                  {www.sce.carleton.ca/~adler/EITv2}
\item \href{https://bitbucket.org/eitbookv2/}
                   {bitbucket.org/eitbookv2/}
\end{itemize}
To contribute via git to the bitbucket.org directly,
please send me your account ID. Both these
sites are public, but are not advertised.



\item {\bf Symbols and Terminology}

It would be nice to use consistent symbols 
across the document. This is most important
for the mathematical chapters  and the lung
and hemodynamics chapters.

Symbols and terminology will be placed in an
appendix (\ref{chap:symbols}).

\end{itemize}


\newpage
\tableofcontents
\newpage

\part{Section: Introduction}
\CHAPPROP{Nontechnical introduction to EIT}
         {Andy Adler, David Holder}
         {overview and introduction aimed at a novice reader}
         {}
         {15--25}

\input{Chap1-introduction.tex}



\part{EIT: tissue properties to image measures}

\CHAPPROP{Tissue Electrical Properties}
         {Rosalind Sadlier, Camelia Gabriel}
         {Tissue Electrical Properties}
         {}
         {15--25}


\input{Chap2-tissue-electrical-properties.tex}



\CHAPPROP{Electronics \& Hardware}
         {Gary Saulnier}
         {Design of EIT electronics}
         {}
         {20--30}
\input{Chap3-electronics-hardware.tex}


\CHAPPROP{The forward problem (EIT Sensitivity)}
         {Andy Adler and William RB Lionheart}
         {Forward modelling and sensitivity of EIT}
         {}
         {35--45}
\input{Chap4-forward-problem.tex}


\CHAPPROP{The inverse problem (Image Reconstruction)}
         {William RB Lionheart, Andrea Borsic, Nick Polydorides}
         {Image reconstruction and the mathematics of the inverse problem}
         {}
         {35--45}
\input{Chap5-inverse-problem.tex}


\CHAPPROP{D-bar algorithms for EIT}
         {David Isaacson, Jennifer L. Mueller, and Samuli Siltanen}
         {D-bar algorithms}
         {}
         {20}
\input{Chap6-dbar-algorithms.tex}

\CHAPPROP{Image Interpretation}
         {Zhanqi Zhao}
         {Post-processing of EIT images and data
          and calculation of EIT-based measures} 
         {}
         {25--35}
\input{Chap7-interpretation.tex}


\part{Applications}
\CHAPPROP{Lungs function}
         {Inéz Frerichs}
         {spontaneous breathing and pulmonary function testing. Other lung-related applications of EIT}
         {}
         {25--35}

\input{Chap8-Lung-Function.tex}

\CHAPPROP{Ventilation monitoring} 
         {Tobias Becher}
         {Monitoring of the ventilated patient}
         {}
         {25--35}
\input{Chap9-Venilation-Monitoring.tex}

\CHAPPROP{Hemodynamics with EIT}
         {Lisa Krukewitt, Stephan B\"ohm,
          Fabian Mueller-Graf, Daniel Reuter}
         {Monitoring of blood flows, pressures and volumes}
         {}
         {25--35}
\input{Chap10-Hemodynamics-EIT.tex}

\CHAPPROP{Nervous tissue: brain and nerves} 
         {Kirill Aristovich and David Holder}
         {EIT monitoring of the brain and nervous tissue}
         {}
         {25--35}
\input{Chap11-Nervous-tissue.tex}

\CHAPPROP{Cancer and Other Clinical Applications} 
         {Ryan Halter}
         {Cancer and other applications such as gastrointestinal}
         {}
         {25--35}
\input{Chap12-Cancer+Others.tex}


\part{Related Technologies}

\CHAPPROP{Magnetic Induction Tomography}
         {Stuart Watson, Hugh Griffiths}
         {MIT and its relation to EIT}
         {}
         {10--20}
\input{Chap13-MIT.tex}

\CHAPPROP{Electrical Impedance Imaging using MRI}
         {Oh In Kwon, Eung Je Woo}
         {MREIT and related technology}
         {}
         {15--25}
\input{Chap14-MREIT.tex}

\CHAPPROP{Geophysical ERT}
         {Alistair Boyle, Paul Wilkinson}
         {Introduction to geophysical ERT and 
          comparison to EIT techniques}
         {}
         {15--25}

\input{Chap15-GeophysicalEIT.tex}



\CHAPPROP{Process tomography}
         {Manuch Soleimani}
         {Process tomography and its relationship/differences to medical EIT}
         {}
         {15--25}
\input{Chap16-Process-Tomography.tex}

\part{Appendices}
\CHAPPROP{Devices, Companies and History}
         {Andy Adler, David Holder}
         {Brief history of EIT developments from the 1980s to 2020}
         {}
         {15--25}
\input{Chap17-Devices-History.tex}

\CHAPPROP{Software, Data and Equipment for EIT}
         {Andy Adler}
         {Overview of Software, Data and Equipment for EIT}
         {}
         {5}
\input{Chap18-Software+Data.tex}

\CHAPPROP{Symbols and terminology}
         {}
         {Symbols and terminology for EIT}
         {}
         {5--10}
\label{chap:symbols}

\input{Chap20-Symbols+Terminology.tex}

\chapter*{References}



\end{document}


