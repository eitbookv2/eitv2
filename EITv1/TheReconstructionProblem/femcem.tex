%\documentclass[11pt,conference]{IEEEtran}
\documentclass[12pt]{article}
\usepackage[dvips] {graphicx}
\usepackage[dvips]{color}
\usepackage{amssymb,amsmath,amstext,latexsym}
\usepackage[normal]{subfigure}
\usepackage{subeqn}

\begin{document}



\section{The finite element method}

On a Lipschitzian domain $\Omega \in \mathbb{R}^3$ with with $L$ electrodes attached on its $C^2$-continuous
boundary $\Gamma$, if no interior sources or charges are present, then the low-frequency, time-harmonic
Maxwell's equations reduce to the elliptic partial differential equation
\begin{equation}\label{pde}
\nabla \cdot (\hat{\sigma} \, \nabla \phi ) = 0 \qquad \text{in} \; \Omega
\end{equation}
where $\phi$ is the scalar electrical potential. The electrical conductivity $\hat{\sigma}$ in $Sm^{-1}$, is
anisotropic within the interior of the domain and it is expressed in the form of the symmetric tensor
\begin{equation}
\hat{\sigma} = \begin{pmatrix}
\sigma^{xx} & \sigma^{xy} & \sigma^{xz}\\
\sigma^{yx} & \sigma^{yy} & \sigma^{yz}\\
\sigma^{zx} & \sigma^{zy} & \sigma^{zz}
\end{pmatrix}
\end{equation}
The equation \eqref{pde} is solved based on the boundary conditions formally known as complete electrode
model \cite{IEEE_PV}. In particular for the boundary current density
\begin{equation}\label{bcn1}
\int_{e_l} \hat{\sigma} \frac{\partial \phi}{\partial \hat{n}} = I_l \qquad \text{on}\; \Gamma_1
\end{equation}
\begin{equation}\label{bcn2}
\hat{\sigma} \frac{\partial \phi}{\partial \hat{n}} = 0 \qquad \text{on}\; \Gamma_2
\end{equation}
hold, while for the boundary electric potential measurements the relation
\begin{equation}\label{bcd}
\phi + z_l\, \hat{\sigma} \frac{\partial \phi}{\partial \hat{n}} = V_l \qquad \text{on}\; \Gamma_1
\end{equation}
is valid. In the equations above, $I_l$ is the current on the surface of the $l$'th electrode $e_l$, $V_l$
the electric potential measured by $e_l$, $z_l$ is the contact impedance of $e_l$, $\hat{n}$ is the unit
normal vector on the boundary of the domain facing outside, and $\Gamma_1 \subset \Gamma$ is the part of the
boundary underneath the electrodes and $\Gamma_2 = \Gamma \setminus \Gamma_2$ is the rest of the surface. The
model is known to have a unique solution \cite{Somersalo} up to an additive constant, thus one can apply some
reference conditions for the potential by grounding one of the electrodes, yielding
\begin{equation}\label{ground}
\phi=0 \Bigl |_{\Gamma_G} \quad \Gamma_G \subset \Gamma_1
\end{equation}
and by considering the charge conservation theorem as well, the condition
\begin{equation}
\sum_{l=1}^L I_l = 0
\end{equation}
is also imposed. When the problem is numerically attempted, the domain is discretized into $n$ vertices
connected into $k$ linear simplices, e.g. tetrahedra elements. For the discrete domain the conductivity
distribution is taken piecewise constant
\begin{equation}
\hat{\sigma}(\Omega) = \sum_{j=1}^k \hat{\sigma}_j \chi^j
\end{equation}
where $\chi^j$ is a basis of piecewise constant functions so that $\chi^j=1$ at the $j$'th element and zero
elsewhere. Similarly, taking $v_i$, $i=1,\ldots,n$ as a basis of piecewise linear functions, then the
potential $\phi$ can be written as an expansion of the basis with weighted coefficients as
\begin{equation}\label{expa}
\phi_h = \sum_{i=1}^n \alpha_i v_i \qquad \phi_h \in L^2(\Omega)
\end{equation}
Multiplying \eqref{pde} by $v$ and integrating over $\Omega$, yields the weak formulation of the problem
\begin{equation}\label{weak}
\int_{\Omega} v \, \nabla \cdot (\hat{\sigma} \nabla \phi) \, dV = 0 \qquad \text{in}\; \Omega
\end{equation}
Using Green's second identity and the vector formula
\begin{equation}
\nabla \cdot ( v \, \hat{\sigma} \nabla \phi) = \hat{\sigma} \nabla \phi \cdot \nabla v + v \nabla \cdot
(\hat{\sigma} \nabla \phi)
\end{equation}
the equation \eqref{weak} is changed to
\begin{equation}
\int_{\Omega} \nabla \cdot ( v \, \hat{\sigma} \nabla \phi)\, dV - \int_{\Omega} \hat{\sigma} \nabla \phi
\cdot \nabla v \, dV = 0
\end{equation}
Invoking the divergence theorem
\begin{equation}
\int_{\Omega} \nabla \cdot (v \, \hat{\sigma} \nabla \phi) \, dV = \int_\Gamma v\, \hat{\sigma} \nabla \phi
\cdot \hat{n} dS
\end{equation}
gives
\begin{equation}
\begin{split}
\int_{\Omega} & \hat{\sigma} \, \nabla \phi \cdot \nabla v \, dV = \int_\Gamma
\hat{\sigma} \nabla \phi \cdot \hat{n} \cdot v \, dS\\
& = \int_{\Gamma_1} \hat{\sigma} \nabla \phi \cdot \hat{n} \cdot v \, dS + \int_{\Gamma_2} \hat{\sigma}
\nabla \phi \cdot \hat{n} \cdot v \, dS
\end{split}
\end{equation}
From the boundary condition \eqref{bcn2}, the surface integral over $\Gamma_2$ is zero thus
\begin{equation}\label{srfint}
\int_{\Omega} \hat{\sigma} \, \nabla \phi \cdot \nabla v \, dV = \int_{\Gamma_1} \hat{\sigma} \nabla \phi
\cdot \hat{n} \cdot v \, dS
\end{equation}
Rearranging the boundary condition \eqref{bcd} as
$$ \hat{\sigma} \nabla \phi \cdot \hat{n} = \frac{1}{z_l}(V_l - \phi) $$
and incorporating it into \eqref{srfint} yields
\begin{equation}
\int_{\Omega} \hat{\sigma} \nabla \phi \cdot \nabla v \, dV = \int_{\Gamma_1} \frac{1}{z_l} (V_l - \phi)\, v
\, dS
\end{equation}
From the expansion of the electric potential the above changes to
\begin{equation}
\int_{\Omega} \hat{\sigma} \nabla \phi \cdot \nabla v \, dV = \int_{\Gamma_1} \frac{1}{z_l} v_i V_l \, dS -
\int_{\Gamma_1} \frac{1}{z_l} \alpha_i v_i \phi_i \, dS
\end{equation}
and substituting for the gradient of $\phi$ from \eqref{expa} into the left hand side integral yields
\begin{equation}\label{fin1}
\alpha_i \biggl \{ \int_\Omega \hat{\sigma} \nabla \phi_i \cdot \nabla v_j \, dV + \int_{\Gamma_1}
\frac{1}{z_l} \phi_i v_j \, dS \biggr \} = V_l \int_{\Gamma_1} \frac{1}{z_l} v_j \, dS
\end{equation}
In a matrix notation, one needs to assemble the local matrices
\begin{subequations}
\begin{equation}
A^{(e)}_m = \int_\Omega \hat{\sigma}^{(e)} \alpha_i \, \nabla \phi_i \cdot \nabla v_j \, dV \qquad \text{for}
\; i,j=1,\ldots,4
\end{equation}
for all the elements in the domain $e=1,\ldots,k$, while the surface integrals
\begin{equation}
A^{(e)}_z = \int_{\Gamma_1} \alpha_i \phi_i v_j \, dS \qquad \text{for} \; i,j=1,\ldots,3
\end{equation}
and
\begin{equation}
A^{(e)}_v = - \int_{\Gamma_1} \frac{1}{z_l} \phi_i \, dS \qquad \text{for} \; i,=1,\ldots,3
\end{equation}
are evaluated for all the elements with a face on $\Gamma_1$,
\end{subequations}
satisfying the matrix equation
\begin{equation}
(A^{(e)}_m + A^{(e)}_z) \, \alpha_i = A^{(e)}_v \, V_l
\end{equation}
Finally, developing the remaining boundary condition \eqref{bcn1} using \eqref{expa} gives
\begin{equation}\label{pf1}
\begin{split}
I_l & = \int_{\Gamma_1} \frac{1}{z_l} (V_l - \phi) \, dS \\
    & = \int_{\Gamma_1} \frac{1}{z_l} V_l - \int_{\Gamma_1} \frac{1}{z_l} \alpha_i v_i \, dS \\
    & = \frac{1}{z_l} V_l \int_{\Gamma_1} dS - \frac{1}{z_l} \alpha_i \int_{\Gamma_1} v_i \, dS\\
    & = \frac{1}{z_l} V_l |e_l| - \frac{1}{z_l} \alpha_i \int_{\Gamma_1} v_i \, dS
\end{split}
\end{equation}
where $|e_l|$ is the total area of the $l$'th electrode evaluated by summing up the individual areas of the
triangular surfaces underneath the electrode. Setting
\begin{equation}\label{pf2}
A^{(l)}_d = \frac{1}{z_l} |e_l| \qquad \text{for} \; l=1,\ldots,L
\end{equation}
and combining \eqref{pf1} and \eqref{pf2} we get
\begin{equation}\label{fin2}
A^{(e)}_v \, \alpha_i + A^{(l)}_d \, V_l = I_l
\end{equation}
so that from \eqref{fin1} and \eqref{fin2} we arrive at the final system
\begin{equation}\label{final}
  \begin{pmatrix}
    A_M+A_Z & A_V \\
    A_V^T & A_D
  \end{pmatrix} \,
  \begin{pmatrix}
    \alpha_i \\
    V_l
  \end{pmatrix} =
  \begin{pmatrix}
    0 \\
    I_l
  \end{pmatrix}
\end{equation}
for $i=1,\ldots,n$ and $l=1,\ldots,L$, where the notation $A_X$ ,$X = M,Z,V$ and $D$ denotes the global
matrix consisted of the contributions of the local matrices $A_x$.


\end{document}
