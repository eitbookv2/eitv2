function mca=meshcorrsp(vtxf,simpf,volf,vtxc,simpc,volc)
%  mca=meshcorrsp(vtxf,simpf,volf,vtxc,simpc)
%
%  Calculates the approximate restriction operator
%  for pw constant fiunctions on a fine simplicial mesh 
%  to a non-conforming course mesh 
%  vtxf vertices of fine mesh
%  simpf simplices of course mesh
%  volf volumes of fine simprahedra
%  vtxc, simpc, volc similarly for course mesh

% (c) WRB Lionheart 2004

[nvf,di] = size(vtxf);
[ntf,dip1]=size(simpf);
[nvc,di]=size(vtxc);
[ntc,dip1]=size(simpc);
mca = sparse([],[],[],ntc,ntf,0);
%mca = zeros(ntc,ntf);
% Make random arrays of homogeneous coords
nrand = 100;
randhom = rand(nrand,di);

for isimpf = 1:ntf
    thisv = vtxf(simpf(isimpf,:),:);
%   Find random points in this simpc
    for ed = 1:di
        edgevec(ed,:)= thisv(ed+1,:)-thisv(1,:);
    end
    rndmcoord = edgevec* randhom'+thisv(1,:)'*ones(1,nrand);
% Which course tetrahedra are these in?
        indxarr = tsearchn(vtxc,simpc,rndmcoord');
% What volume of this simpf in each simpc ?
        for ir=1:length(indxarr);
         indir =indxarr(ir);   
         if ~isnan(indir)  % NaN if not in any           
            mca(indir,isimpf)=mca(indir,isimpf)+(volf(isimpf)/nrand)/volc(indir);
         end   
        end
% must be a neater way to do that!
end        