function vtxn=centres(vtx,tri)
% vtxn=centres(vtx,tri)
[ntri,d]=size(tri);
for k=1:ntri
    vtxl = tri(k,:);
    vtxn(k,:)= mean(vtx(vtxl,:));
end