
\section{Total Variation Regularization}
The
Total Variation functional is assuming an important role in the regularisation of inverse problems belonging to
many disciplines, after its first introduction by Rudin, Osher and Fatemi (1992) \cite{rudin92} in the image
restoration context. The use of such a functional as a regularisation penalty term allows the reconstruction of
discontinuous profiles. As this is a desirable property, the method is gaining popularity.

Total variation measures the total amplitude of the oscillations of a
function. For a differentiable function on a domain $\Omega$
 the total variation is
\cite{dobson97}
\begin{equation}\label{eq:non-smooth:TV def}
TV(f)=\int_{\Omega} |\nabla f |
\end{equation}
The definition can be extended to non--differentiable functions \cite{giusti84} as:
\begin{equation}\label{eq:non-smooth:TV def non-diff}
    TV(f)=\sup\limits_{\myvec{v} \in \mathcal{V}}\int_{\Omega} f \; \mathrm{div} \, \vec{v}
\end{equation}
where $\mathcal{V}$ is the space of continuously differentiable functions that vanish on $\partial
\Omega$ and $\|\vec{v}\|_{\Omega} \leq 1$.

As the TV functional measures the variations of a function over its domain, it can be understood to be effective
at reducing oscillations in the inverted profile, if used as a penalty term. The same properties apply however
to $\ell_2$ regularisation functionals. The important difference is that the class of functions with bounded
total variation also includes discontinuous functions, which makes the TV particularly attractive for the
regularisation of non--smooth profiles. The following one-dimensional example illustrates the advantage of using
the TV against a quadratic functional in non-smooth contexts \vspace{0.5cm}

Let $F=\{f:[0,1]\rightarrow \mathbb{R}, | \, f(0)=a, f(1)=b\}$, we have
\begin{itemize}
    \item $\min\limits_{f \in F} \, \int_0^1 |f'(x)| \mathrm{d}x$ is achieved by any monotonic
    function, including discontinuous ones.
    \item $\min\limits_{f \in F} \, \int_0^1 (f'(x))^2 \mathrm{d}x$ is achieved only by the straight
    line connecting the points $(0,a)\,(1,b)$.
\end{itemize}

\begin{figure}
  \centering
  \resizebox{7cm}{!}{\includegraphics{1DTVExample.pdf}}
  \caption{Three possible functions: $f_1,f_2,f_3 \in F$. All of them have the same
   TV, but only $f_2$ minimises the $H^1$ semi-norm.}\label{fig:non-smooth:1DTVExample}
\end{figure}
Figure \ref{fig:non-smooth:1DTVExample} shows three possible functions $f_1, f_2, f_3$ in $F$. All of them have
the same total variation, including $f_3$ which is discontinuous. Only $f_2$ however minimises the $H^1$
semi--norm
\begin{equation}\label{eq:non-smoothintro:h1seminorm}
    |f|_{H_1}=\left(\int_0^1\left(\frac{\partial f}{\partial x}\right)^2\mathrm{d}x \right)^{1/2}
\end{equation}
which is a semi--norm, as $|f|_{H_1}=0$ does not imply that $f=0$. The quadratic functional, if used as
penalty, would therefore bias the inversion toward the linear solution and the function $f_3$ would not be
admitted in the solution set as its $H^1$ semi-norm is infinite.


Two different approaches
proposed for application of TV to EIT, the first by Dobson \textit{et. al.} \cite{dobson94} and the
second by Somersalo \textit{et. al.} and Kolehmainen \textit{et. al.} \cite{somersalo97} \cite{kolehmainen01}.
The approach proposed by Dobson and Santosa is suitable for the linearised problem and suffers from poor
numerical efficiency. Somersalo and Kolehmainen successfully applied MCMC methods  to solve the TV regularised inverse problem. The advantage in applying MCMC methods
over deterministic methods is that they do not suffer from the numerical problems involved with
non-differentiability of the TV functional. They do not require \emph{ad hoc} techniques. Probabilistic methods,
such as MCMC, offer central estimates and errors bars by sampling the posterior probability density of the
sought parameters. The sampling process involves a substantial computational effort, often the inverse problem
is linearised in order to speed up the sampling. 
What is required is  an efficient method for deterministic Tikhonov
style regularisation, to offer a non--linear TV regularised inversion
in a short time. We will briefly describe the Primal Dual Interior
Point Method (PD-IPM) to TV applied to EIT~\cite{AndreaWC,AndreaTh}
which is just such a method. In Sec~\ref{sec:numerTV} we present some
numerical results using this method for the first time for 3D EIT.

A second aspect, which adds importance to the study of efficient MAP (Tikhonov) methods, is that the
linearisation in MCMC methods is usually performed after an initial MAP guess. Kolehmainen \cite{kolehmainen01}
reports calculating several iterations of a Newton method before starting the burn-in phase of his algorithm. A
good initial deterministic TV inversion could therefore bring benefit to these approaches.

Examining the relevant literature, a variety of deterministic numerical methods have been used for the
regularisation of image denoising and restoration problems with the TV functional (a good review is offered by
Vogel in \cite{vogel02}). The numerical efficiency and stability are the main issues to be addressed. Use of
\emph{ad--hoc} techniques is common, given the poor performance of traditional algorithms. Most of the
deterministic methods draw from ongoing research in optimisation, as TV minimisation belongs to the important
classes of problems known as ``Minimisation of sum of norms'' \cite{coleman92} \cite{andersen95}
\cite{andersen00} and ``Linear $\ell_1$ problems'' \cite{barrodale78} \cite{xue00}.


\subsection{Duality for Tikhonov Regularised Inverse Problems}

In inverse problems, with linear forward operators, the discretised TV regularised inverse problem, can be
formulated as
\begin{equation}
\label{eq:non-smooth:tomography primal} (P) \quad \min\limits_\x \frac{1}{2}\|\A\x-\b\|^2 + \alpha \|L\,\x\|
\end{equation}
We will label it as the primal problem. A {\em Dual} problem to
(P), which can be shown to be equivalent~\cite{AndreaTh} is
\begin{equation}\label{eq:non-smooth:tomography dual1}
(D) \quad \max\limits_{\y:\|\y\|\leq1} \; \;  \min\limits_\x \frac{1}{2}\|\A\x-\b\|^2 +
\alpha \, \y^T\L\x
\end{equation}
The optimisation problem
\begin{equation}\label{eq:non-smooth:tomography dual2}
\min\limits_\x \frac{1}{2}\|\A\x-\b\|^2 + \alpha \,  \y^T\L\x
\end{equation}
has an optimal point defined by the first order conditions
\begin{equation}\label{eq:non-smooth:tomography dual3}
\A^T(\A\x-\b) + \alpha \,  \L^T\mathbf{\x}=0
\end{equation}
the dual problem can be written therefore as
\begin{equation}\label{eq:non-smooth:tomography dual4}
(D) \quad \max\limits_{\begin{array}{c}
\mathbf{y}:\|\mathbf{y}\|\leq 1 \\
A^T(A\x-\b) + \alpha \,  L^T\mathbf{y}=0
\end{array}} \hspace{-10mm}  \frac{1}{2}\|\A\x-\b\|^2 + \alpha \,
\mathbf{y}^T\L\x
\end{equation}
The complementarity condition for \eqref{eq:non-smooth:tomography primal} and
\eqref{eq:non-smooth:tomography dual4} is set by nulling the primal dual gap
\begin{equation}\label{eq:non-smooth:tomography complementarity1}
 \frac{1}{2}\|\A\x-\b\|^2 + \alpha \,  \|L\,\x\|-\frac{1}{2}\|A\x-\b\|^2 - \alpha
  \y^T\L\x=0 
\end{equation}
which with the dual feasibility $\|\y\|\leq 1$ is equivalent to requiring that
\begin{equation}\label{eq:non-smooth:tomography complementarity2}
\L\x-\|\L \x\|\y=0 
\end{equation}
in analogy with the findings of the previous paragraph. The PD-IPM framework for the TV
regularised inverse problem can thus be written as
\begin{subequations}\label{eq:non-smooth:PD-IPM framework tomo}
\begin{equation}\label{eq:non-smooth:PD-IPM framework primal feas invpb}
    \|\y\|\leq 1
\end{equation}
\begin{equation}\label{eq:non-smooth:PD-IPM framework dual feas invpb}
    A^T(A\x-\b) + \alpha \,  L^T\y=0
\end{equation}
\begin{equation}\label{eq:non-smooth:PD-IPM framework complement invpb}
    \L\x-\|\L \x\|\y=0 
\end{equation}
\end{subequations}
Again it is not possible to apply the Newton Method directly to \eqref{eq:non-smooth:PD-IPM framework tomo} as
\eqref{eq:non-smooth:PD-IPM framework complement invpb} is not differentiable for $\L \x=0$. A centering
condition has to be applied, obtaining a smooth pair of optimisation problems (P$_\beta$) and (D$_\beta$) and a
central path parameterised by $\beta$. This is done by replacing $\|\L\x\|$ by
$(\|\L\x\|^2+\beta)^{\frac{1}{2}}$ in \eqref{eq:non-smooth:PD-IPM framework complement invpb}.

\subsection{Application to EIT}

The PD-IPM algorithm in its original form \cite{chan95-2} was developed for inverse problems with linear forward
operators, as non--linear problems have not yet been addressed by research in this field at the time of this
writing. The following section describes the numerical implementation for inversion in EIT tomography. The
implementation is based on the results of the duality theory for inverse problems with linear forward operators.
The extension of the theoretical results to non--linear inversion is out of the scope of the present work. It
was possible to apply the original algorithm to the EIT inverse problem with minor modifications, and to obtain
successful reconstructions. The formulation for the EIT inverse problem is

\begin{equation}
\label{eq:non-smooth:EIT TV regularised2}
\begin{array}{c}
\sigmav_{rec}=\argmin \psi(\sigmav) \\
\psi(\sigmav)=\frac{1}{2} \|h(\sigmav)-\b\|^2 + \alpha \, TV(\sigmav)
\end{array}
\end{equation}
With a similar notation as used in Section \ref{sec:non-smooth:primal methods}, the system of non--linear
equations that defines the PD--IPM method for \eqref{eq:non-smooth:EIT TV regularised2} can be written as
\begin{equation}\label{eq:non-smooth:PD-IPM for EIT inverse problem}
    \begin{array}{c}
      \|\y\| \leq 1 \\
      J^T(h(\sigmav)-\b)+\alpha\,L^T\y=0 \\
      L\sigmav-E\y=0
    \end{array}
\end{equation}
with $\eta_i=\sqrt{\|\L \, \sigmav\|^2+\beta}$ and $E=\mathrm{diag}(\eta_i)$, and $J$ the Jacobian of the
forward operator $h(\sigmav)$. Newton's method can be applied to solve \eqref{eq:non-smooth:PD-IPM for EIT
inverse problem} obtaining the following system for the updates $\delta\sigmav$ and $\delta\y$ of the
primal and dual variables
\begin{equation}\label{eq:non-smooth:PD-IPM for EIT inverse problem Newton's method}
    \left [
    \begin{array}{cc}
     J^TJ & \alpha\,L^T \\
     \overline{F}L & -E
    \end{array}
    \right ]
    \left [
    \begin{array}{c}
      \delta\sigmav \\
      \delta\y
    \end{array}
    \right ]= -
    \left [
    \begin{array}{c}
     J^T(h(\sigmav)-\b) + \alpha\,L^T\y\\
     L\sigmav-E\y
    \end{array}
    \right ]
\end{equation}
with
\begin{equation}\label{eq:non-smooth:newton matrix F-bar}
\overline{F}=\mathrm{diag}(1-\frac{x_i\L \, \sigmav}{\eta_i})
\end{equation}
which in turn can be solved as follows
\begin{subequations}
\label{eq:non-smooth:EIT primal dual updates}
\begin{eqnarray}
      [J^TJ+\alpha\,L^TE^{-1}\overline{F}L] \, \delta\sigmav&=&-[J^T(h(\sigmav)-\b)+\alpha\,L^TE^{-1}L \, \sigmav] \label{eq:non-smooth:EIT primal update} \\
      \delta\y&=&-\y+E^{-1}L\sigmav+E^{-1}\overline{F}L \, \delta\sigmav
\end{eqnarray}
\end{subequations}
Equations \eqref{eq:non-smooth:EIT primal dual updates} can therefore be applied iteratively to solve the
non--linear inversion \eqref{eq:non-smooth:EIT TV regularised2}. Some care must be taken on the dual variable
update, to maintain dual feasibility. A traditional line search procedure with feasibility checks is not
suitable as the dual update direction is not guaranteed to be an ascent direction for the penalised dual
objective function $(D_\beta)$. The simplest way to compute the update is called the \emph{scaling rule}
\cite{andersen00} which is defined to work as follows
\begin{equation}\label{eq:non-smooth:scaling rule}
    \y^{(k+1)}=\lambda(\y^{(k)}+\delta \y^{(k)})
\end{equation}
where
\begin{equation}\label{eq:non-smooth:lambda_x 1}
   \lambda=\max \{\lambda: \;
   \lambda\|\y_i^{(k)}+\delta\y_i^{(k)}\| \leq 1, \quad i=1,\ldots,n \}
\end{equation}
An alternative way is to calculate the exact step length to the boundary, applying what is
called the \emph{steplength rule} \cite{andersen00}
\begin{equation}\label{eq:non-smooth:steplength rule}
    \y^{(k+1)}=\y^{(k)}+\min(1,\lambda)\, \delta \y^{(k)}
\end{equation}
where
\begin{equation}\label{eq:non-smooth:lambda_x 2}
   \lambda=\max \{\lambda: \;
   \|\y_i^{(k)}+\lambda \, \delta\y_i^{(k)}\| \leq 1, \quad i=1,\ldots,n \}
\end{equation}
In the context of EIT, and in tomography in general, the computation involved in calculating the exact step
length to the boundary of the dual feasibility region is negligible compared to the whole algorithm iteration.
It is convenient therefore to adopt the exact update, which in our experiments resulted in a better convergence.
The scaling rule has the further disadvantage of always placing $\y$ on the boundary of the feasible
region, which prevents the algorithm from following the central path. Concerning the updates on the primal
variable, the update direction $\delta\sigmav$ is a descent direction for $(P_\beta)$ therefore a line search
procedure could be opportune. In our numerical experiments we have found that for relatively small contrasts
(e.g. 3:1) the primal line search procedure is not needed, as the steps are unitary. For larger contrasts a line
search on the primal variable guarantees the stability of the algorithm.

As a general comment, it is important to note that equation \eqref{eq:non-smooth:EIT primal update} is identical
to the update equation for the primal Newton's method \eqref{eq:non-smooth:discrete newton iteration} except for
the presence of $\overline{F}$ in place of $F$; secondly $\overline{F} \rightarrow F$ as the solution is
approached. Furthermore as the dual variable is usually started from the feasible initial point $\y=0$,
the matrix $\overline{F}$ is initially the identity matrix. The PD--IPM method can thus be considered as an
interpolation between the Lagged diffusivity method and Newton's Method for the primal problem. Initially the
algorithm behaves like the Lagged Diffusivity method, which is stable, and as the optimal point is approached
the algorithm behaves like Newton's Method, which converges quadratically (at least for linear forward operators
\cite{chan95-2}) in a region around the solution. The method therefore shows stability and efficiency
\cite{andersen00},\cite{chan95-2},\cite{borsic01b} (see also Section \ref{sec:non-smooth:pd-ipm numerical
experiments}) at the small additional cost (over primal methods) of computing the updates of the dual variables.

\subsection{Comments on the Duality for the Continuous Problem}

Some insight on the greater efficiency of primal dual methods over primal methods can be
gained from some observations reported by Chan and Mulet in \cite{chan95-2} and \cite{chan96}.
One major difficulty associated with finding an optimal point for the $TV$ regularised inverse
problem \eqref{eq:non-smooth:TVbeta regularised} is that the associated Euler-Lagrange
equation
\begin{equation}\label{eq:non-smooth:tv diffusion eq2}
A^T(A f - d)-\alpha \, \nabla \cdot \left ( \frac{\nabla f}{\sqrt{|\nabla f|^2 + \beta}} \right )=0
\end{equation}
contains the highly non--linear term $\nabla \cdot \left (\frac{\nabla f}{\sqrt{|\nabla f|^2 + \beta}}  \right
)$. The linearisation of equation \eqref{eq:non-smooth:tv diffusion eq2} and its solution by Newton's method is
therefore likely to have a very small convergence region. A better way to solve \eqref{eq:non-smooth:tv
diffusion eq2} is to note that $\frac{\nabla f}{|\nabla f|}$ is the unit normal vector to the level sets of $f$,
and would, in general, be smoother than $\nabla \cdot \frac{\nabla f}{|\nabla f|}$. An auxiliary variable
$w=\frac{\nabla f}{|\nabla f|}$ can thus be introduced and the Euler-Lagrange equation solved by means of this
equivalent system of partial differential equations
\begin{subequations}\label{eq:non-smooth:duality for continuous case}
\begin{eqnarray}
A^T(A f - d)-\alpha \, \nabla \cdot w &=&0 \\
\nabla f-|\nabla f |w &=&0
\end{eqnarray}
\end{subequations}
which is ``more linear'' than \eqref{eq:non-smooth:tv diffusion eq2} and thus better suited to
be solved via linearisation techniques. By inspection, equations \eqref{eq:non-smooth:duality
for continuous case} can be thought to be the continuous equivalent to the discrete system of
equations \eqref{eq:non-smooth:PD-IPM framework tomo}. The primal dual method can therefore be
expected to achieve better results thanks to a better linearisation of the problem.

