# README #


### What is this repository for? ###

* Preparation and version control of the EIT v2 book

### Contribution guidelines ###

* Co-authors have been invited for this book

### Whom do I talk to? ###

* Andy Adler <adler@sce.carleton.ca>
* David Holder <holder@ucl.ac.uk>
